## Table of contents

- [\Cdt\Console\Application](#class-cdtconsoleapplication)
- [\Cdt\Console\Command\AbstractCommand (abstract)](#class-cdtconsolecommandabstractcommand-abstract)
- [\Cdt\Console\Command\InfoCommand](#class-cdtconsolecommandinfocommand)
- [\Cdt\Console\Command\Generate\EntityCommand](#class-cdtconsolecommandgenerateentitycommand)
- [\Cdt\Console\Command\Generate\GenerateCommand](#class-cdtconsolecommandgenerategeneratecommand)
- [\Cdt\Console\Command\Generate\GenerateRequestCommand](#class-cdtconsolecommandgenerategeneraterequestcommand)
- [\Cdt\Console\Command\Generate\GenerateResponseCommand](#class-cdtconsolecommandgenerategenerateresponsecommand)
- [\Cdt\Console\Command\Generate\GenerateServiceCommand](#class-cdtconsolecommandgenerategenerateservicecommand)
- [\Cdt\Console\Command\Generate\InitCommand](#class-cdtconsolecommandgenerateinitcommand)
- [\Cdt\Console\Command\Generate\StructureCommand](#class-cdtconsolecommandgeneratestructurecommand)
- [\Cdt\Console\Command\Generate\Create\GenerateRequestCreateCommand](#class-cdtconsolecommandgeneratecreategeneraterequestcreatecommand)
- [\Cdt\Console\Command\Generate\Create\GenerateResponseCreateCommand](#class-cdtconsolecommandgeneratecreategenerateresponsecreatecommand)
- [\Cdt\Console\Command\Generate\Create\GenerateServiceCreateCommand](#class-cdtconsolecommandgeneratecreategenerateservicecreatecommand)
- [\Cdt\Console\Command\Generate\Get\GenerateRequestGetCommand](#class-cdtconsolecommandgenerategetgeneraterequestgetcommand)
- [\Cdt\Console\Command\Generate\Get\GenerateResponseGetCommand](#class-cdtconsolecommandgenerategetgenerateresponsegetcommand)
- [\Cdt\Console\Command\Generate\Get\GenerateServiceGetCommand](#class-cdtconsolecommandgenerategetgenerateservicegetcommand)
- [\Cdt\Console\Config\Locator](#class-cdtconsoleconfiglocator)

***** 
### Class: \Cdt\Console\Application

> Class BaseApplication

| Visibility | Function |
|:-----------|:---------|
| public | **__construct(***string* **$name=`'UNKNOWN'`**, *string* **$version=`'UNKNOWN'`**, *mixed* **$rootPath=null**, *string* **$configFile=`''`**, *[\Cdt\Console\Config\Locator](#class-cdtconsoleconfiglocator)* **$configLocator=null**)** : *void* |
| public | **getConfig()** : *array/string* |
| public | **getConfigFile()** : *string* |
| public | **getConfigLocator()** : *string* |
| public | **getConfigPath()** : *string* |
| public | **getRootPath()** : *string* |
| public | **setConfig(***string* **$config**)** : *void* |
| public | **setConfigFile(***string* **$configFile**)** : *void* |
| public | **setConfigLocator(***string* **$configLocator**)** : *void* |
| public | **setConfigPath(***string* **$configPath**)** : *void* |
| public | **setRootPath(***string* **$rootPath**)** : *void* |
| protected | **loadDefaultConfig()** : *bool* |
| protected | **loadDefaults()** : *mixed*<br />*Boilerplate configuration loader* |

*This class extends \Symfony\Component\Console\Application*

***** 
### Class: \Cdt\Console\Command\AbstractCommand (abstract)

> Class AbstractCommand

| Visibility | Function |
|:-----------|:---------|
| protected | **configure()** : *void*<br />*Generic set up function* |
| protected | **executeCommand(***mixed* **$commandName**, *mixed* **$commandArguments**, *\Symfony\Component\Console\Output\OutputInterface* **$output**)** : *bool/int* |
| protected | **generateCamelName(***string* **$name=`''`**)** : *string* |
| protected | **generateCanonicalName(***string* **$name=`''`**)** : *string* |
| protected | **generateExtension()** : *string* |
| protected | **validateRequiredDefinitions(***\Symfony\Component\Console\Input\InputInterface* **$input**, *\Symfony\Component\Console\Output\OutputInterface* **$output**)** : *array* |
| protected | **validateTypeFunction(***mixed* **$type**, *string* **$function=`'add'`**)** : *string* |
| protected | **writeMapType(***mixed* **$doctrineType**)** : *string* |

*This class extends \Symfony\Component\Console\Command\Command*

***** 
### Class: \Cdt\Console\Command\InfoCommand

> Class InfoCommand

| Visibility | Function |
|:-----------|:---------|
| protected | **configure()** : *void* |
| protected | **execute(***\Symfony\Component\Console\Input\InputInterface* **$input**, *\Symfony\Component\Console\Output\OutputInterface* **$output**)** : *bool* |

*This class extends [\Cdt\Console\Command\AbstractCommand](#class-cdtconsolecommandabstractcommand-abstract)*

***** 
### Class: \Cdt\Console\Command\Generate\EntityCommand

> Class EntityCommand

| Visibility | Function |
|:-----------|:---------|
| protected | **configure()** : *void* |
| protected | **execute(***\Symfony\Component\Console\Input\InputInterface* **$input**, *\Symfony\Component\Console\Output\OutputInterface* **$output**)** : *bool* |
| protected | **generateEntityColumn(***mixed* **$entityColumnType**)** : *string* |
| protected | **generateEntityFooter()** : *string* |
| protected | **generateEntityGetter(***mixed* **$entityColumnType**)** : *bool/string* |
| protected | **generateEntityHeader(***mixed* **$entityName**)** : *string* |
| protected | **generateEntityName(***mixed* **$name**)** : *string* |
| protected | **generateEntityPath(***mixed* **$entityName**)** : *string* |
| protected | **generateEntitySetter(***mixed* **$entityColumnType**)** : *bool/string* |
| protected | **generateEntityTest(***mixed* **$entityName**, *mixed* **$entityTestName**)** : *string* |
| protected | **generateEntityTestName(***mixed* **$name**)** : *string* |
| protected | **generateEntityTestPath(***mixed* **$entityName**)** : *string* |
| protected | **writeEntityColumn(***mixed* **$entityColumn**, *string* **$type=`''`**)** : *string* |
| protected | **writeEntityGetter(***mixed* **$entityColumn**, *mixed* **$type**)** : *string* |
| protected | **writeEntitySetter(***mixed* **$entityColumn**, *mixed* **$type**)** : *string* |

*This class extends [\Cdt\Console\Command\AbstractCommand](#class-cdtconsolecommandabstractcommand-abstract)*

***** 
### Class: \Cdt\Console\Command\Generate\GenerateCommand

> Class GenerateCommand

| Visibility | Function |
|:-----------|:---------|
| protected | **configure()** : *void* |
| protected | **execute(***\Symfony\Component\Console\Input\InputInterface* **$input**, *\Symfony\Component\Console\Output\OutputInterface* **$output**)** : *bool* |
| protected | **mapEntityColumn(***\Zend\Db\Metadata\Object\ColumnObject* **$column**)** : *string* |
| protected | **mapRequestColumn(***\Zend\Db\Metadata\Object\ColumnObject* **$column**)** : *string* |
| protected | **mapServiceColumn(***\Zend\Db\Metadata\Object\ColumnObject* **$column**)** : *string* |

*This class extends [\Cdt\Console\Command\AbstractCommand](#class-cdtconsolecommandabstractcommand-abstract)*

***** 
### Class: \Cdt\Console\Command\Generate\GenerateRequestCommand

> Class GenerateRequestCommand

| Visibility | Function |
|:-----------|:---------|
| protected | **configure()** : *void* |
| protected | **execute(***\Symfony\Component\Console\Input\InputInterface* **$input**, *\Symfony\Component\Console\Output\OutputInterface* **$output**)** : *bool* |

*This class extends [\Cdt\Console\Command\AbstractCommand](#class-cdtconsolecommandabstractcommand-abstract)*

***** 
### Class: \Cdt\Console\Command\Generate\GenerateResponseCommand

> Class GenerateResponseCommand

| Visibility | Function |
|:-----------|:---------|
| protected | **configure()** : *void* |
| protected | **execute(***\Symfony\Component\Console\Input\InputInterface* **$input**, *\Symfony\Component\Console\Output\OutputInterface* **$output**)** : *bool* |

*This class extends [\Cdt\Console\Command\AbstractCommand](#class-cdtconsolecommandabstractcommand-abstract)*

***** 
### Class: \Cdt\Console\Command\Generate\GenerateServiceCommand

> Class GenerateServiceCommand

| Visibility | Function |
|:-----------|:---------|
| protected | **configure()** : *void* |
| protected | **execute(***\Symfony\Component\Console\Input\InputInterface* **$input**, *\Symfony\Component\Console\Output\OutputInterface* **$output**)** : *bool* |

*This class extends [\Cdt\Console\Command\AbstractCommand](#class-cdtconsolecommandabstractcommand-abstract)*

***** 
### Class: \Cdt\Console\Command\Generate\InitCommand

> Class InitCommand

| Visibility | Function |
|:-----------|:---------|
| protected | **callbackStructureIterator(***\Symfony\Component\Console\Output\OutputInterface* **$output**, *mixed* **$array**, *mixed* **$callback**, *mixed* **$treePos**, *string* **$treeName=`''`**)** : *void* |
| protected | **configure()** : *void* |
| protected | **execute(***\Symfony\Component\Console\Input\InputInterface* **$input**, *\Symfony\Component\Console\Output\OutputInterface* **$output**)** : *bool* |

*This class extends [\Cdt\Console\Command\AbstractCommand](#class-cdtconsolecommandabstractcommand-abstract)*

***** 
### Class: \Cdt\Console\Command\Generate\StructureCommand

> Class StructureCommand

| Visibility | Function |
|:-----------|:---------|
| protected | **configure()** : *void* |
| protected | **execute(***\Symfony\Component\Console\Input\InputInterface* **$input**, *\Symfony\Component\Console\Output\OutputInterface* **$output**)** : *bool* |
| protected | **generateStructureColumn(***mixed* **$structureColumnType**)** : *string* |
| protected | **generateStructureFooter()** : *string* |
| protected | **generateStructureHeader(***mixed* **$structureName**)** : *string* |
| protected | **generateStructureName(***mixed* **$name**)** : *string* |
| protected | **generateStructurePath()** : *string* |
| protected | **writeStructureColumn(***mixed* **$structureColumn**, *string* **$type=`''`**)** : *string* |

*This class extends [\Cdt\Console\Command\AbstractCommand](#class-cdtconsolecommandabstractcommand-abstract)*

***** 
### Class: \Cdt\Console\Command\Generate\Create\GenerateRequestCreateCommand

> Class GenerateRequestGetCommand

| Visibility | Function |
|:-----------|:---------|
| protected | **configure()** : *void* |
| protected | **execute(***\Symfony\Component\Console\Input\InputInterface* **$input**, *\Symfony\Component\Console\Output\OutputInterface* **$output**)** : *bool* |
| protected | **generateRequestColumn(***mixed* **$requestColumnType**)** : *string* |
| protected | **generateRequestFooter()** : *string* |
| protected | **generateRequestGetter(***mixed* **$requestColumnType**)** : *bool/string* |
| protected | **generateRequestHeader(***mixed* **$requestEntity**, *mixed* **$requestService**, *mixed* **$requestType**)** : *string* |
| protected | **generateRequestName(***mixed* **$requestService**, *mixed* **$requestType**)** : *string* |
| protected | **generateRequestPath(***mixed* **$requestEntity**, *mixed* **$requestService**, *mixed* **$requestType**)** : *string* |
| protected | **generateRequestSetter(***mixed* **$requestColumnType**)** : *bool/string* |
| protected | **generateRequestTestFooter()** : *string* |
| protected | **generateRequestTestHeader(***mixed* **$requestName**, *mixed* **$requestTestName**, *mixed* **$requestColumns**, *mixed* **$entity**, *mixed* **$service**, *mixed* **$type**)** : *string* |
| protected | **generateRequestTestHeaderName(***mixed* **$requestService**, *mixed* **$requestType**)** : *string* |
| protected | **generateRequestTestHeaderPath(***mixed* **$requestEntity**, *mixed* **$requestService**, *mixed* **$requestType**)** : *string* |
| protected | **generateRequestTestInvalidData(***mixed* **$requestColumns**)** : *string* |
| protected | **generateRequestTestValidData(***mixed* **$requestColumns**)** : *string* |
| protected | **generateRequestValidationFooter()** : *string* |
| protected | **generateRequestValidationHeader()** : *string* |
| protected | **generateRequestValidationRule(***mixed* **$requestColumn**)** : *bool/string* |
| protected | **writeRequestColumn(***mixed* **$requestColumn**, *string* **$type=`''`**)** : *string* |
| protected | **writeRequestGetter(***mixed* **$requestColumn**, *mixed* **$type**)** : *string* |
| protected | **writeRequestSetter(***mixed* **$requestColumn**, *mixed* **$type**)** : *string* |

*This class extends [\Cdt\Console\Command\AbstractCommand](#class-cdtconsolecommandabstractcommand-abstract)*

***** 
### Class: \Cdt\Console\Command\Generate\Create\GenerateResponseCreateCommand

| Visibility | Function |
|:-----------|:---------|
| protected | **configure()** : *void* |
| protected | **execute(***\Symfony\Component\Console\Input\InputInterface* **$input**, *\Symfony\Component\Console\Output\OutputInterface* **$output**)** : *bool* |
| protected | **generateResponseFooter()** : *string* |
| protected | **generateResponseHeader(***mixed* **$responseEntity**, *mixed* **$responseService**, *mixed* **$responseType**)** : *string* |
| protected | **generateResponseName(***mixed* **$responseService**, *mixed* **$responseType**)** : *string* |
| protected | **generateResponsePath(***mixed* **$responseEntity**, *mixed* **$responseService**, *mixed* **$responseType**)** : *string* |

*This class extends [\Cdt\Console\Command\AbstractCommand](#class-cdtconsolecommandabstractcommand-abstract)*

***** 
### Class: \Cdt\Console\Command\Generate\Create\GenerateServiceCreateCommand

> Class GenerateServiceCreateCommand

| Visibility | Function |
|:-----------|:---------|
| protected | **configure()** : *void* |
| protected | **execute(***\Symfony\Component\Console\Input\InputInterface* **$input**, *\Symfony\Component\Console\Output\OutputInterface* **$output**)** : *bool* |
| protected | **generateFixtureData(***mixed* **$serviceColumns**)** : *void* |
| protected | **generateRequestName(***mixed* **$serviceService**, *mixed* **$serviceType**)** : *string* |
| protected | **generateResponseName(***mixed* **$serviceService**, *mixed* **$serviceType**)** : *string* |
| protected | **generateService(***mixed* **$serviceEntity**, *mixed* **$serviceService**, *mixed* **$serviceType**)** : *string* |
| protected | **generateServiceCoreFooter()** : *string* |
| protected | **generateServiceCoreHeader(***mixed* **$serviceEntity**, *mixed* **$serviceService**, *mixed* **$serviceType**)** : *string* |
| protected | **generateServiceCoreName(***mixed* **$serviceService**, *mixed* **$serviceType**)** : *string* |
| protected | **generateServiceCorePath(***mixed* **$serviceEntity**, *mixed* **$serviceService**, *mixed* **$serviceType**)** : *string* |
| protected | **generateServiceCoreTestFooter()** : *void* |
| protected | **generateServiceCoreTestHeader(***mixed* **$serviceEntity**, *mixed* **$serviceService**, *mixed* **$serviceType**)** : *string* |
| protected | **generateServiceCoreTestMakeRequest(***mixed* **$serviceService**, *mixed* **$serviceType**)** : *void* |
| protected | **generateServiceCoreTestName(***mixed* **$serviceService**, *mixed* **$serviceType**)** : *string* |
| protected | **generateServiceCoreTestPath(***mixed* **$serviceEntity**, *mixed* **$serviceService**, *mixed* **$serviceType**)** : *string* |
| protected | **generateServiceName(***mixed* **$serviceService**, *mixed* **$serviceType**)** : *string* |
| protected | **generateServicePath(***mixed* **$serviceEntity**, *mixed* **$serviceService**, *mixed* **$serviceType**)** : *string* |

*This class extends [\Cdt\Console\Command\AbstractCommand](#class-cdtconsolecommandabstractcommand-abstract)*

***** 
### Class: \Cdt\Console\Command\Generate\Get\GenerateRequestGetCommand

> Class GenerateRequestGetCommand

| Visibility | Function |
|:-----------|:---------|
| protected | **configure()** : *void* |
| protected | **execute(***\Symfony\Component\Console\Input\InputInterface* **$input**, *\Symfony\Component\Console\Output\OutputInterface* **$output**)** : *bool* |
| protected | **generateRequestColumn(***mixed* **$requestColumnType**)** : *string* |
| protected | **generateRequestFooter()** : *string* |
| protected | **generateRequestGetter(***mixed* **$requestColumnType**)** : *bool/string* |
| protected | **generateRequestHeader(***mixed* **$requestEntity**, *mixed* **$requestService**, *mixed* **$requestType**)** : *string* |
| protected | **generateRequestName(***mixed* **$requestService**, *mixed* **$requestType**)** : *string* |
| protected | **generateRequestPath(***mixed* **$requestEntity**, *mixed* **$requestService**, *mixed* **$requestType**)** : *string* |
| protected | **generateRequestSetter(***mixed* **$requestColumnType**)** : *bool/string* |
| protected | **generateRequestTestFooter()** : *string* |
| protected | **generateRequestTestHeader(***mixed* **$requestName**, *mixed* **$requestTestName**, *mixed* **$entity**, *mixed* **$service**, *mixed* **$type**)** : *string* |
| protected | **generateRequestTestHeaderName(***mixed* **$requestService**, *mixed* **$requestType**)** : *string* |
| protected | **generateRequestTestHeaderPath(***mixed* **$requestEntity**, *mixed* **$requestService**, *mixed* **$requestType**)** : *string* |
| protected | **generateRequestTestInvalidData(***mixed* **$requestColumns**)** : *string* |
| protected | **generateRequestTestValidData(***mixed* **$requestColumns**)** : *string* |
| protected | **generateRequestValidationFooter()** : *string* |
| protected | **generateRequestValidationHeader()** : *string* |
| protected | **generateRequestValidationRule(***mixed* **$requestColumn**)** : *bool/string* |
| protected | **writeRequestColumn(***mixed* **$requestColumn**, *string* **$type=`''`**)** : *string* |
| protected | **writeRequestGetter(***mixed* **$requestColumn**, *mixed* **$type**)** : *string* |
| protected | **writeRequestSetter(***mixed* **$requestColumn**, *mixed* **$type**)** : *string* |

*This class extends [\Cdt\Console\Command\AbstractCommand](#class-cdtconsolecommandabstractcommand-abstract)*

***** 
### Class: \Cdt\Console\Command\Generate\Get\GenerateResponseGetCommand

| Visibility | Function |
|:-----------|:---------|
| protected | **configure()** : *void* |
| protected | **execute(***\Symfony\Component\Console\Input\InputInterface* **$input**, *\Symfony\Component\Console\Output\OutputInterface* **$output**)** : *bool* |
| protected | **generateResponseFooter()** : *string* |
| protected | **generateResponseHeader(***mixed* **$responseEntity**, *mixed* **$responseService**, *mixed* **$responseType**)** : *string* |
| protected | **generateResponseName(***mixed* **$responseService**, *mixed* **$responseType**)** : *string* |
| protected | **generateResponsePath(***mixed* **$responseEntity**, *mixed* **$responseService**, *mixed* **$responseType**)** : *string* |

*This class extends [\Cdt\Console\Command\AbstractCommand](#class-cdtconsolecommandabstractcommand-abstract)*

***** 
### Class: \Cdt\Console\Command\Generate\Get\GenerateServiceGetCommand

> Class GenerateServiceCommand

| Visibility | Function |
|:-----------|:---------|
| protected | **configure()** : *void* |
| protected | **execute(***\Symfony\Component\Console\Input\InputInterface* **$input**, *\Symfony\Component\Console\Output\OutputInterface* **$output**)** : *bool* |
| protected | **generateRequestName(***mixed* **$serviceService**, *mixed* **$serviceType**)** : *string* |
| protected | **generateResponseName(***mixed* **$serviceService**, *mixed* **$serviceType**)** : *string* |
| protected | **generateService(***mixed* **$serviceEntity**, *mixed* **$serviceService**, *mixed* **$serviceType**)** : *string* |
| protected | **generateServiceCoreCondition(***mixed* **$serviceColumn**)** : *void* |
| protected | **generateServiceCoreConditionsFooter()** : *string* |
| protected | **generateServiceCoreConditionsHeader(***mixed* **$serviceService**, *mixed* **$serviceType**)** : *string* |
| protected | **generateServiceCoreFooter()** : *string* |
| protected | **generateServiceCoreHeader(***mixed* **$serviceEntity**, *mixed* **$serviceService**, *mixed* **$serviceType**)** : *string* |
| protected | **generateServiceCoreName(***mixed* **$serviceService**, *mixed* **$serviceType**)** : *string* |
| protected | **generateServiceCorePath(***mixed* **$serviceEntity**, *mixed* **$serviceService**, *mixed* **$serviceType**)** : *string* |
| protected | **generateServiceCoreTestExampleResponse(***mixed* **$serviceColumns**)** : *void* |
| protected | **generateServiceCoreTestFooter()** : *void* |
| protected | **generateServiceCoreTestHeader(***mixed* **$serviceEntity**, *mixed* **$serviceService**, *mixed* **$serviceType**)** : *string* |
| protected | **generateServiceCoreTestLookupData(***mixed* **$serviceColumns**)** : *void* |
| protected | **generateServiceCoreTestName(***mixed* **$serviceService**, *mixed* **$serviceType**)** : *string* |
| protected | **generateServiceCoreTestPath(***mixed* **$serviceEntity**, *mixed* **$serviceService**, *mixed* **$serviceType**)** : *string* |
| protected | **generateServiceName(***mixed* **$serviceService**, *mixed* **$serviceType**)** : *string* |
| protected | **generateServicePath(***mixed* **$serviceEntity**, *mixed* **$serviceService**, *mixed* **$serviceType**)** : *string* |

*This class extends [\Cdt\Console\Command\AbstractCommand](#class-cdtconsolecommandabstractcommand-abstract)*

***** 
### Class: \Cdt\Console\Config\Locator

> Class Locator

| Visibility | Function |
|:-----------|:---------|
| public | **loadConfig(***mixed* **$path**, *mixed* **$file**)** : *array/string* |

