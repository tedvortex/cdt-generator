<?php
namespace Cdt\Console\Config;

use Symfony\Component\Config\Exception\FileLoaderLoadException;
use Symfony\Component\Config\FileLocator;
use Yosymfony\ConfigLoader\Config;
use Yosymfony\ConfigLoader\Loaders\JsonLoader;

/**
 * Class Locator
 *
 * @package Cdt\ServiceLayer\Console\Config
 */
class Locator
{
    /**
     * @param $path
     * @param $file
     *
     * @return array|string
     */
    public function loadConfig($path, $file)
    {
        $locator = new FileLocator($path);

        $config = new Config(
            [
                new JsonLoader($locator),
            ]
        );

        try {
            $repository = $config->load($file);

            return $repository;
        } catch (FileLoaderLoadException $e) {
            return $e;
        }
    }
}
