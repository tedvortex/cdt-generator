<?php
namespace Cdt\Console;

use Cdt\Console\Config\Locator;

/**
 * Class BaseApplication
 *
 * @package Cdt\ServiceLayer\Console
 */
class Application extends \Symfony\Component\Console\Application
{
    /**
     * @type string
     */
    protected $config;
    /**
     * @type string
     */
    protected $configLocator;
    /**
     * @type string
     */
    protected $configPath;
    /**
     * @type string
     */
    protected $configFile;
    /**
     * @type string
     */
    protected $rootPath;

    /**
     * @param string  $name
     * @param string  $version
     * @param null    $rootPath
     * @param string  $configFile
     * @param Locator $configLocator
     */
    public function __construct(
        $name = 'UNKNOWN',
        $version = 'UNKNOWN',
        $rootPath = null,
        $configFile = '',
        Locator $configLocator = null
    ) {
        parent::__construct(
            $name,
            $version
        );

        $this->setConfigLocator($configLocator);
        $this->setConfigPath($rootPath);
        $this->setConfigFile($configFile);

        $this->loadDefaults();
    }

    /**
     * @return string
     */
    public function getConfigLocator()
    {
        return $this->configLocator;
    }

    /**
     * @param string $configLocator
     */
    public function setConfigLocator($configLocator)
    {
        $this->configLocator = $configLocator;
    }

    /**
     * @return string
     */
    public function getConfigPath()
    {
        return $this->configPath;
    }

    /**
     * @param string $configPath
     */
    public function setConfigPath($configPath)
    {
        $this->configPath = $configPath;
    }

    /**
     * @return string
     */
    public function getConfigFile()
    {
        return $this->configFile;
    }

    /**
     * @param string $configFile
     */
    public function setConfigFile($configFile)
    {
        $this->configFile = $configFile;
    }

    /**
     * @return string
     */
    public function getRootPath()
    {
        return $this->rootPath;
    }

    /**
     * @param string $rootPath
     */
    public function setRootPath($rootPath)
    {
        $this->rootPath = $rootPath;
    }

    /**
     * @return array|string
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param string $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }

    /**
     * Boilerplate configuration loader
     */
    protected function loadDefaults()
    {
        if ($this->loadDefaultConfig()) {
            if (isset($this->config["global"]["path"])) {
                $this->setRootPath($this->config["global"]["path"]);
            }
        }
    }

    /**
     * @return bool
     */
    protected function loadDefaultConfig()
    {
        /**
         * @var Locator $configLocator
         */
        $configLocator = $this->getConfigLocator();
        if (null !== $configLocator) {
            $config = $configLocator->loadConfig(
                $this->getConfigPath(),
                $this->getConfigFile()
            );

            if (false !== $config) {
                $this->setConfig($config);

                return true;
            }
        }

        return false;
    }
}
