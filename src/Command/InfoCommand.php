<?php
namespace Cdt\Console\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class InfoCommand
 *
 * @package Cdt\Console\Command
 */
class InfoCommand extends AbstractCommand
{
    /**
     * @type string
     */
    protected $name = "info";
    /**
     * @type string
     */
    protected $description = "Show basic information and available generator commands";
    /**
     * @type array
     */
    protected $availableCommands = [
        "generate",
        "generate:init",
        "generate:entity",
        "generate:structure",
        "generate:request",
        "generate:response",
        "generate:service",
    ];

    /**
     * @internal
     */
    protected function configure()
    {
        parent::configure();

        $this->setHelp(
            "The <info>$this->name</info> command shows basic information about existing commands and their usage"
        );
    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface   $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     *
     * @return bool
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $app = $this->getApplication();

        $output->writeln("<info>Welcome to the " . $app->getName() . " version " . $app->getVersion() . "</info>");
        $output->writeln("<comment>These  are the currently available commands:</comment>");
        foreach ($this->availableCommands as $command) {
            $output->writeln(" <info>" . $command . "</info>");
        }

        return true;
    }
}
