<?php
namespace Cdt\Console\Command\Generate\Get;

use Cdt\Console\Application;
use Cdt\Console\Command\AbstractCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\Container;

/**
 * Class GenerateRequestGetCommand
 *
 * @package Cdt\Console\Command\Generate\Get
 */
class GenerateRequestGetCommand extends AbstractCommand
{
    /**
     * The type of service, provided by the top level generate command
     */
    const TYPE = "get";
    /**
     * @type string
     */
    protected $name = "generate:request:get";
    /**
     * @type string
     */
    protected $description = "Generate a service get request based on given parameters";

    /**
     * @type array
     */
    protected $requiredDefinitions = [
        [
            "name"        => "entity",
            "description" => "Top level service namespace",
            "validation"  => "Please specify a top level service entity namespace",
            "default"     => null,
        ],
        [
            "name"        => "service",
            "description" => "Top level service entity",
            "validation"  => "Please specify a top level service name",
            "default"     => null,
        ],
        [
            "name"        => "path",
            "description" => "Path where to save the service relative to the cli call",
            "validation"  => "Please specify a path",
            "default"     => "src/Service/",
        ],
        [
            "name"        => "columns",
            "type"        => "argument",
            "class"       => 4,
            "description" => "Service entity columns",
            "validation"  => "Please specify all the columns divided by spaces",
            "default"     => null,
        ],
    ];

    /**
     * @internal
     */
    protected function configure()
    {
        parent::configure();

        $this->setHelp(
            "The <info>" . $this->name . "</info> command creates a service get request from a given set of parameters"
        );
    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface   $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     *
     * @return bool
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->validateRequiredDefinitions($input, $output);

        if (! $this->error) {
            $requestPath = $this->generateRequestPath(
                $input->getOption("entity"),
                $input->getOption("service"),
                $this::TYPE
            );
            $requestTestPath = $this->generateRequestTestHeaderPath(
                $input->getOption("entity"),
                $input->getOption("service"),
                $this::TYPE
            );
            $requestName = $this->generateRequestName(
                $input->getOption("service"),
                $this::TYPE
            );
            $requestTestName = $this->generateRequestTestHeaderName(
                $input->getOption("service"),
                $this::TYPE
            );
            $requestColumns = $requestTestColumns = $setterColumns = $getterColumns = $validationColumns = $input->getArgument("columns");
            $requestColumns[] = $setterColumns[] = $getterColumns[] = $requestTestColumns[] = "order_by:string:order";

            if (! is_dir($requestPath)) {
                mkdir($requestPath);
            }
            $requestStream = fopen(
                $requestPath . $requestName . $this->generateExtension(),
                "w"
            );

            if ($requestStream) {
                fwrite(
                    $requestStream,
                    $this->generateRequestHeader(
                        $input->getOption("entity"),
                        $input->getOption("service"),
                        $this::TYPE
                    )
                );

                foreach ($requestColumns as $requestColumn) {
                    fwrite(
                        $requestStream,
                        $this->generateRequestColumn($requestColumn)
                    );
                }

                foreach ($getterColumns as $requestColumn) {
                    fwrite(
                        $requestStream,
                        $this->generateRequestGetter($requestColumn)
                    );
                }

                foreach ($setterColumns as $requestColumn) {
                    fwrite(
                        $requestStream,
                        $this->generateRequestSetter($requestColumn)
                    );
                }

                fwrite(
                    $requestStream,
                    $this->generateRequestValidationHeader()
                );

                foreach ($validationColumns as $requestColumn) {
                    fwrite(
                        $requestStream,
                        $this->generateRequestValidationRule($requestColumn)
                    );
                }

                fwrite(
                    $requestStream,
                    $this->generateRequestValidationFooter()
                );

                fwrite(
                    $requestStream,
                    $this->generateRequestFooter()
                );
            }

            if (! is_dir($requestTestPath)) {
                mkdir($requestTestPath);
            }
            $requestTestStream = fopen(
                $requestTestPath . $requestTestName . $this->generateExtension(),
                "w"
            );

            if ($requestTestStream) {
                fwrite(
                    $requestTestStream,
                    $this->generateRequestTestHeader(
                        $requestName,
                        $requestTestName,
                        $input->getOption("entity"),
                        $input->getOption("service"),
                        $this::TYPE
                    )
                );

                fwrite(
                    $requestTestStream,
                    $this->generateRequestTestValidData($requestColumns)
                );

                fwrite(
                    $requestTestStream,
                    $this->generateRequestTestInvalidData($requestColumns)
                );

                fwrite(
                    $requestTestStream,
                    $this->generateRequestTestFooter()
                );
            }
        }

        return ! $this->error;
    }

    /**
     * @param $requestEntity
     * @param $requestService
     * @param $requestType
     *
     * @return string
     */
    protected function generateRequestPath($requestEntity, $requestService, $requestType)
    {
        /**
         * @var Application $app
         */
        $app = $this->getApplication();
        $entity = $this->generateCanonicalName($requestEntity);
        $service = $this->generateCanonicalName($requestService);
        $type = $this->generateCanonicalName($requestType);

        return $app->getRootPath() . "/src/Service/" . $entity . "/" . $service . "/" . $type . "/V1/";
    }

    /**
     * @param $requestEntity
     * @param $requestService
     * @param $requestType
     *
     * @return string
     */
    protected function generateRequestTestHeaderPath($requestEntity, $requestService, $requestType)
    {
        return $this->generateRequestPath(
            $requestEntity,
            $requestService,
            $requestType
        ) . "Tests/";
    }

    /**
     * @param $requestService
     * @param $requestType
     *
     * @return string
     */
    protected function generateRequestName($requestService, $requestType)
    {
        $service = $this->generateCanonicalName($requestService);
        $type = $this->generateCanonicalName($requestType);

        return $type . $service . "Request";
    }

    /**
     * @param $requestService
     * @param $requestType
     *
     * @return string
     */
    protected function generateRequestTestHeaderName($requestService, $requestType)
    {
        return $this->generateRequestName(
            $requestService,
            $requestType
        ) . "Test";
    }

    /**
     * @param $requestEntity
     * @param $requestService
     * @param $requestType
     *
     * @return string
     */
    protected function generateRequestHeader($requestEntity, $requestService, $requestType)
    {
        $entity = $this->generateCanonicalName($requestEntity);
        $service = $this->generateCanonicalName($requestService);
        $type = $this->generateCanonicalName($requestType);
        $requestName = $this->generateRequestName(
            $requestService,
            $requestType
        );

        return <<<EOT
<?php
namespace Cdt\\Service\\$entity\\$service\\$type\\V1;

use Cdt\\Common\\Request\\AbstractPaginatedRequest;
use Cdt\\Common\\Request\\Request;
use Symfony\\Component\\Validator\\Mapping\\ClassMetadata;
use Symfony\\Component\\Validator\\Constraints as Assert;

/**
 * Class $requestName
 * @package Cdt\\Service\\$entity\\$service\\$type\\V1
 */
class $requestName extends AbstractPaginatedRequest implements Request
{
EOT;
    }

    /**
     * @param $requestColumnType
     *
     * @return string
     */
    protected function generateRequestColumn($requestColumnType)
    {
        if (preg_match(
            "%^([a-zA-Z\_]+)\:([a-zA-Z]+)(?:\:([a-zA-Z]+))?%is",
            $requestColumnType,
            $matches
        )) {
            return $this->writeRequestColumn(
                $matches[1],
                $matches[2]
            );
        }

        return false;
    }

    /**
     * @param        $requestColumn
     * @param string $type
     *
     * @return string
     */
    protected function writeRequestColumn($requestColumn, $type = "")
    {
        return PHP_EOL . <<<EOT
    /**
     * @type $type
     */
    private \$$requestColumn;

EOT;
    }

    /**
     * @param $requestColumnType
     *
     * @return bool|string
     */
    protected function generateRequestGetter($requestColumnType)
    {
        if (preg_match(
            "%^([a-zA-Z\_]+):([a-zA-Z]+)%is",
            $requestColumnType,
            $matches
        )) {
            return $this->writeRequestGetter(
                $matches[1],
                $matches[2]
            );
        }

        return false;
    }

    /**
     * @param $requestColumn
     * @param $type
     *
     * @return string
     */
    protected function writeRequestGetter($requestColumn, $type)
    {
        $requestColumnCamel = Container::camelize($requestColumn);

        return PHP_EOL . <<<EOT
    /**
     * @return $type
     */
    public function get$requestColumnCamel()
    {
        return \$this->$requestColumn;
    }

EOT;
    }

    /**
     * @param $requestColumnType
     *
     * @return bool|string
     */
    protected function generateRequestSetter($requestColumnType)
    {
        if (preg_match(
            "%^([a-zA-Z\_]+):([a-zA-Z]+)%is",
            $requestColumnType,
            $matches
        )) {
            return $this->writeRequestSetter(
                $matches[1],
                $matches[2]
            );
        }

        return false;
    }

    /**
     * @param $requestColumn
     * @param $type
     *
     * @return string
     */
    protected function writeRequestSetter($requestColumn, $type)
    {
        $requestColumnCamel = Container::camelize($requestColumn);

        return PHP_EOL . <<<EOT
    /**
     * @param $type \$$requestColumn
     */
    public function set$requestColumnCamel(\$$requestColumn)
    {
        \$this->$requestColumn = \$$requestColumn;
    }

EOT;
    }

    /**
     * @return string
     */
    protected function generateRequestFooter()
    {
        return <<<EOT
}

EOT;
    }

    /**
     * @return string
     */
    protected function generateRequestValidationHeader()
    {
        return PHP_EOL . <<<EOT
    /**
     * @param \Symfony\Component\Validator\Mapping\ClassMetadata \$metadata
     */
    public static function configureValidator(ClassMetadata \$metadata)
    {
        \$metadata->setGroupSequence(['type', \$metadata->getDefaultGroup()]);

EOT;
    }

    /**
     * @param $requestColumn
     *
     * @return bool|string
     */
    protected function generateRequestValidationRule($requestColumn)
    {
        if (preg_match(
            "%^([a-zA-Z\_]+):([a-zA-Z]+)%is",
            $requestColumn,
            $matches
        )) {
            $column = $matches[1];
            $entityColumn = $this->generateCamelName($column);
            $type = $matches[2];

            return PHP_EOL . <<<EOT
        // $column validation
        \$metadata->addGetterConstraints(
            "$entityColumn",
            [
                new Assert\\Type(
                    [
                        "type"   => "$type",
                        "groups" => ["type"]
                    ]
                ),
            ]
        );

EOT;
        }

        return false;
    }

    /**
     * @return string
     */
    protected function generateRequestValidationFooter()
    {
        return PHP_EOL . <<<EOT
        \$metadata->addGetterConstraints(
            'orderBy',
            [
                new Assert\Collection(
                    [
                        'fields' => [
                            'dateCreated' => new Assert\Optional(
                                [
                                    new Assert\NotBlank(),
                                    new Assert\Choice(['ASC', 'DESC']),
                                ]
                            ),
                        ],
                        'allowMissingFields' => true
                    ]
                ),
            ]
        );
    }

EOT;
    }

    /**
     * @param $requestName
     * @param $requestTestName
     * @param $entity
     * @param $service
     * @param $type
     *
     * @return string
     */
    protected function generateRequestTestHeader($requestName, $requestTestName, $entity, $service, $type)
    {
        $entity = $this->generateCanonicalName($entity);
        $service = $this->generateCanonicalName($service);
        $type = $this->generateCanonicalName($type);

        return <<<EOT
<?php
namespace Cdt\\Service\\$entity\\$service\\$type\\V1\\Tests;

use Cdt\\Common\\Testing\\Unit\\Request\\RequestTestTrait;
use Cdt\\Common\\Testing\\Unit\\Request\\RequestWithSimpleDataTestsTrait;
use Cdt\\Service\\$entity\\$service\\$type\\V1\\$requestName;
use PHPUnit_Framework_TestCase;

class $requestTestName extends PHPUnit_Framework_TestCase
{
    use RequestTestTrait;
    use RequestWithSimpleDataTestsTrait;

    public function getClassToTest()
    {
        return $requestName::class;
    }

    public function getValidRequest()
    {
        \$request = new $requestName();

        return \$request;
    }

EOT;
    }

    /**
     * @param $requestColumns
     * @return string
     */
    protected function generateRequestTestValidData($requestColumns)
    {
        $output = PHP_EOL . <<<EOT
    /**
     * @return array
     */
    public function getSimpleValidRequestsData()
    {
        return [
EOT;

        foreach ($requestColumns as $requestColumn) {
            if (preg_match(
                "%^([a-zA-Z\_]+):([a-zA-Z]+):([a-zA-Z]+)%is",
                $requestColumn,
                $matches
            )) {
                $field = $this->generateCamelName($matches[1]);
                $type = $matches[2];
                $module = $matches[3];
                $validValue = null;

                $output .= <<<EOT
EOT;

                $output .= PHP_EOL . <<<EOT
            ["$field", null],
EOT;

                if ($module == "order") {
                    $output .= PHP_EOL . <<<EOT
            ["$field", []],
            ["$field", ["dateCreated" => "ASC"]],
            ["$field", ["dateCreated" => "DESC"]],
EOT;
                } else {
                    switch ($type) {
                        case "integer":
                            $validValue = 1;
                            break;
                        case "string":
                            $validValue = '"test-text"';
                            break;
                        case "datetime":
                            $validValue = '"' . date("Y-m-d H:i:s", $this::TIME) . '"';
                            break;
                    }

                    $output .= PHP_EOL . <<<EOT
            ["$field", $validValue],
EOT;
                }
            }
        }

        $output .= <<<EOT

        ];
    }

EOT;

        return $output;
    }

    /**
     * @param $requestColumns
     * @return string
     */
    protected function generateRequestTestInvalidData($requestColumns)
    {
        $output = PHP_EOL . <<<EOT
    /**
     * @return array
     */
    public function getSimpleInvalidRequestsData()
    {
        return [
EOT;

        foreach ($requestColumns as $requestColumn) {
            if (preg_match(
                "%^([a-zA-Z\_]+):([a-zA-Z]+):([a-zA-Z]+)%is",
                $requestColumn,
                $matches
            )) {
                $field = $this->generateCamelName($matches[1]);
                $type = $matches[2];
                $module = $matches[3];
                $validValue = null;

                $output .= <<<EOT
EOT;

                if ($module == "order") {
                    $output .= PHP_EOL . <<<EOT
            ["$field", [""]],
            ["$field", [[]]],
            ["$field", [false]],
            ["$field", [0]],
            ["$field", ["incorrectKey" => "ASC"]],
            ["$field", ["dateCreated" => "value"]],
EOT;
                } else {
                    switch ($type) {
                        case "integer":
                            $output .= PHP_EOL . <<<EOT
            ["$field", [""]],
            ["$field", "example text #24d&^"],
            ["$field", false],
EOT;
                            break;

                        case "string":
                            $output .= PHP_EOL . <<<EOT
            ["$field", [""]],
            ["$field", 123],
            ["$field", false],
EOT;
                            break;

                        case "datetime":
                            $output .= PHP_EOL . <<<EOT
            ["$field", [""]],
            ["$field", 123],
            ["$field", false],
EOT;
                            break;
                    }
                }
            }
        }

        $output .= <<<EOT

        ];
    }

EOT;

        return $output;
    }

    /**
     * @return string
     */
    protected function generateRequestTestFooter()
    {
        return <<<EOT
}

EOT;
    }
}
