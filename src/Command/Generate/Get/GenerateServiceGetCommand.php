<?php
namespace Cdt\Console\Command\Generate\Get;

use Cdt\Console\Application;
use Cdt\Console\Command\AbstractCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\Container;

/**
 * Class GenerateServiceCommand
 *
 * @package Cdt\Console\Command\Generate
 */
class GenerateServiceGetCommand extends AbstractCommand
{
    /**
     * The type of service, provided by the top level generate command
     */
    const TYPE = "get";
    /**
     * @type string
     */
    protected $name = "generate:service:get";
    /**
     * @type string
     */
    protected $description = "Generate a get service core based on given parameters";

    /**
     * @type array
     */
    protected $requiredDefinitions = [
        [
            "name"        => "entity",
            "description" => "Top level service namespace",
            "validation"  => "Please specify a top level service entity namespace",
            "default"     => null,
        ],
        [
            "name"        => "service",
            "description" => "Top level service entity",
            "validation"  => "Please specify a top level service name",
            "default"     => null,
        ],
        [
            "name"        => "path",
            "description" => "Path where to save the service relative to the cli call",
            "validation"  => "Please specify a path",
            "default"     => "src/Service/",
        ],
        [
            "name"        => "columns",
            "type"        => "argument",
            "class"       => 4,
            "description" => "Service entity columns",
            "validation"  => "Please specify all the columns divided by spaces",
            "default"     => null,
        ],
    ];

    /**
     * @internal
     */
    protected function configure()
    {
        parent::configure();

        $this->setHelp(
            "The <info>" . $this->name . "</info> command creates a service core from a given set of parameters"
        );
    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface   $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     *
     * @return bool
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->validateRequiredDefinitions($input, $output);

        if (! $this->error) {
            $servicePath = $this->generateServicePath(
                $input->getOption("entity"),
                $input->getOption("service"),
                $this::TYPE
            );
            $serviceCorePath = $this->generateServiceCorePath(
                $input->getOption("entity"),
                $input->getOption("service"),
                $this::TYPE
            );
            $serviceCoreTestPath = $this->generateServiceCoreTestPath(
                $input->getOption("entity"),
                $input->getOption("service"),
                $this::TYPE
            );
            $serviceName = $this->generateServiceName(
                $input->getOption("service"),
                $this::TYPE
            );
            $serviceCoreName = $this->generateServiceCoreName(
                $input->getOption("service"),
                $this::TYPE
            );
            $serviceCoreTestName = $this->generateServiceCoreTestName(
                $input->getOption("service"),
                $this::TYPE
            );
            $serviceColumns = $input->getArgument("columns");

            if (! is_dir($servicePath)) {
                mkdir($servicePath);
            }
            $serviceStream = fopen(
                $servicePath . $serviceName . $this->generateExtension(),
                "w"
            );

            if ($serviceStream) {
                fwrite(
                    $serviceStream,
                    $this->generateService(
                        $input->getOption("entity"),
                        $input->getOption("service"),
                        $this::TYPE
                    )
                );
            }

            if (! is_dir($serviceCorePath)) {
                mkdir($serviceCorePath);
            }
            $serviceCoreStream = fopen(
                $serviceCorePath . $serviceCoreName . $this->generateExtension(),
                "w"
            );

            if ($serviceCoreStream) {
                fwrite(
                    $serviceCoreStream,
                    $this->generateServiceCoreHeader(
                        $input->getOption("entity"),
                        $input->getOption("service"),
                        $this::TYPE
                    )
                );

                fwrite(
                    $serviceCoreStream,
                    $this->generateServiceCoreConditionsHeader(
                        $input->getOption("service"),
                        $this::TYPE
                    )
                );

                foreach ($input->getArgument("columns") as $column) {
                    fwrite(
                        $serviceCoreStream,
                        $this->generateServiceCoreCondition($column)
                    );
                }

                fwrite(
                    $serviceCoreStream,
                    $this->generateServiceCoreConditionsFooter()
                );

                fwrite(
                    $serviceCoreStream,
                    $this->generateServiceCoreFooter()
                );
            }

            if (! is_dir($serviceCoreTestPath)) {
                mkdir($serviceCoreTestPath);
            }
            $serviceCoreTestStream = fopen(
                $serviceCoreTestPath . $serviceCoreTestName . $this->generateExtension(),
                "w"
            );

            if ($serviceCoreTestStream) {
                fwrite(
                    $serviceCoreTestStream,
                    $this->generateServiceCoreTestHeader(
                        $input->getOption("entity"),
                        $input->getOption("service"),
                        $this::TYPE
                    )
                );

                fwrite(
                    $serviceCoreTestStream,
                    $this->generateServiceCoreTestLookupData($serviceColumns)
                );

                fwrite(
                    $serviceCoreTestStream,
                    $this->generateServiceCoreTestExampleResponse($serviceColumns)
                );

                fwrite(
                    $serviceCoreTestStream,
                    $this->generateServiceCoreTestFooter()
                );
            }
        }

        return ! $this->error;
    }

    /**
     * @param $serviceEntity
     * @param $serviceService
     * @param $serviceType
     *
     * @return string
     */
    protected function generateServicePath($serviceEntity, $serviceService, $serviceType)
    {
        /**
         * @var Application $app
         */
        $app = $this->getApplication();
        $entity = $this->generateCanonicalName($serviceEntity);
        $service = $this->generateCanonicalName($serviceService);
        $type = $this->generateCanonicalName($serviceType);

        return $app->getRootPath() . "/src/Service/" . $entity . "/" . $service . "/" . $type . "/V1/";
    }

    /**
     * @param $serviceEntity
     * @param $serviceService
     * @param $serviceType
     *
     * @return string
     */
    protected function generateServiceCorePath($serviceEntity, $serviceService, $serviceType)
    {
        return $this->generateServicePath(
            $serviceEntity,
            $serviceService,
            $serviceType
        );
    }

    /**
     * @param $serviceEntity
     * @param $serviceService
     * @param $serviceType
     *
     * @return string
     */
    protected function generateServiceCoreTestPath($serviceEntity, $serviceService, $serviceType)
    {
        return $this->generateServicePath(
            $serviceEntity,
            $serviceService,
            $serviceType
        ) . "/Tests/";
    }

    /**
     * @param $serviceService
     * @param $serviceType
     *
     * @return string
     */
    protected function generateServiceName($serviceService, $serviceType)
    {
        $service = $this->generateCanonicalName($serviceService);
        $type = $this->generateCanonicalName($serviceType);

        return $type . $service . "Service";
    }

    /**
     * @param $serviceService
     * @param $serviceType
     *
     * @return string
     */
    protected function generateRequestName($serviceService, $serviceType)
    {
        $service = $this->generateCanonicalName($serviceService);
        $type = $this->generateCanonicalName($serviceType);

        return $type . $service . "Request";
    }

    /**
     * @param $serviceService
     * @param $serviceType
     *
     * @return string
     */
    protected function generateResponseName($serviceService, $serviceType)
    {
        $service = $this->generateCanonicalName($serviceService);
        $type = $this->generateCanonicalName($serviceType);

        return $type . $service . "Response";
    }

    /**
     * @param $serviceService
     * @param $serviceType
     *
     * @return string
     */
    protected function generateServiceCoreName($serviceService, $serviceType)
    {
        return $this->generateServiceName(
            $serviceService,
            $serviceType
        ) . "Core";
    }

    /**
     * @param $serviceService
     * @param $serviceType
     *
     * @return string
     */
    protected function generateServiceCoreTestName($serviceService, $serviceType)
    {
        return $this->generateServiceName(
            $serviceService,
            $serviceType
        ) . "CoreTest";
    }

    /**
     * @param $serviceEntity
     * @param $serviceService
     * @param $serviceType
     *
     * @return string
     */
    protected function generateServiceCoreHeader($serviceEntity, $serviceService, $serviceType)
    {
        $entity = $this->generateCanonicalName($serviceEntity);
        $service = $this->generateCanonicalName($serviceService);
        $type = $this->generateCanonicalName($serviceType);
        $serviceName = $this->generateServiceName(
            $serviceService,
            $serviceType
        );
        $serviceCoreName = $this->generateServiceCoreName(
            $serviceService,
            $serviceType
        );
        $serviceRequestName = $this->generateRequestName(
            $serviceService,
            $serviceType
        );
        $serviceResponseName = $this->generateResponseName(
            $serviceService,
            $serviceType
        );

        return <<<EOT
<?php
namespace Cdt\\Service\\$entity\\$service\\$type\\V1;

use Cdt\\Common\\Service\\AbstractSimpleGetEntityService;
use Cdt\\Gateway\\Query\\Simple\\SimpleReadQuery;
use Cdt\\Gateway\\Simple\\SimpleReadRepository;
use Cdt\\Utils\\Event\\EventDispatcher;
use Cdt\\Utils\\Factory\\Factory;
use Symfony\\Component\\Validator\\Validator\\ValidatorInterface;

/**
 * Class $serviceCoreName
 * @package Cdt\\Service\\$entity\\$service\\$type\\V1
 */
class $serviceCoreName extends AbstractSimpleGetEntityService implements $serviceName
{
    /**
     * @type SimpleReadRepository
     */
    private \$repository;

    public function __construct(
        EventDispatcher \$eventDispatcher,
        Factory \$responseFactory,
        ValidatorInterface \$validator,
        SimpleReadRepository \$repository,
        Factory \$entityFactory
    ) {
        parent::__construct(
            \$eventDispatcher,
            \$responseFactory,
            \$validator,
            \$repository,
            \$entityFactory
        );

        \$this->repository = \$repository;
    }

    /**
     * @param $serviceRequestName \$request
     *
     * @return $serviceResponseName
     */
    public function handleRequest($serviceRequestName \$request)
    {
        \$query = \$this->repository->newSimpleReadQuery();

        \$this->addQueryConditionsFromRequest(
            \$query,
            \$request
        );

        \$query->setLimit(\$request->getLimit());
        \$query->setStart(\$request->getStart());

        if (\$request->getOrderBy() !== null) {
            foreach (\$request->getOrderBy() as \$field => \$ordering) {
                \$ascending = true;
                if (\$ordering === "DESC") {
                    \$ascending = false;
                }

                \$query->addSimpleOrdering(\$field, \$ascending);
            }
        }

        /**
         * @param $serviceResponseName \$response
         */
        \$data = \$this->repository->executeSimpleReadQuery(\$query);
        \$pagination = \$this->repository->executeSimpleCountQuery(\$query);

        return \$this->buildResponseFromQueryResults(
            \$data,
            \$pagination
        );
    }

EOT;
    }

    /**
     * @return string
     */
    protected function generateServiceCoreFooter()
    {
        return <<<EOT

    /**
     * @param \$result
     *
     * @return mixed
     */
    protected function buildEntityFromResult(\$result)
    {
        \$entity = \$this->createEntity();

        return \$entity->createFromArray(\$result);
    }
}

EOT;
    }

    /**
     * @param $serviceService
     * @param $serviceType
     *
     * @return string
     */
    protected function generateServiceCoreConditionsHeader($serviceService, $serviceType)
    {
        $serviceRequestName = $this->generateRequestName(
            $serviceService,
            $serviceType
        );

        return <<<EOT

    /**
     * @param SimpleReadQuery \$query
     * @param $serviceRequestName \$request
     *
     * @return SimpleReadQuery
     */
    protected function addQueryConditionsFromRequest(SimpleReadQuery \$query, \$request)
    {
        \$filters = [

EOT;
    }

    /**
     * @return string
     */
    protected function generateServiceCoreConditionsFooter()
    {
        return <<<EOT
        ];

        foreach (\$filters as \$k => \$filter) {
            if (! empty(\$filter) && null !== \$filter) {
                \$query->addSimpleCondition(
                    \$k,
                    \$query->equalTo(),
                    \$filter
                );
            }
        }

        return \$query;
    }

EOT;
    }

    protected function generateServiceCoreCondition($serviceColumn)
    {
        if (preg_match(
            "%^([a-zA-Z\_]+)\:%is",
            $serviceColumn,
            $matches
        )) {
            $column = Container::camelize($matches[1]);
            $columnKey = mb_strtolower($column[0]) . substr($column, 1);

            return <<<EOT
            "$columnKey" => \$request->get$column(),

EOT;
        }

        return false;
    }

    /**
     * @param $serviceEntity
     * @param $serviceService
     * @param $serviceType
     *
     * @return string
     */
    protected function generateService($serviceEntity, $serviceService, $serviceType)
    {
        $entity = $this->generateCanonicalName($serviceEntity);
        $service = $this->generateCanonicalName($serviceService);
        $type = $this->generateCanonicalName($serviceType);
        $serviceName = $this->generateServiceName(
            $serviceService,
            $serviceType
        );
        $serviceRequest = $this->generateRequestName(
            $serviceService,
            $serviceType
        );
        $serviceResponse = $this->generateResponseName(
            $serviceService,
            $serviceType
        );

        return <<<EOT
<?php
namespace Cdt\\Service\\$entity\\$service\\$type\\V1;

use Cdt\\Common\\Service\\Service;

 /**
 * Interface $serviceName
 * The $serviceName service returns a list of "$service" type entities on a defined limit/start and optional filters
 * @package Cdt\\Service\\$entity\\$service\\$type\\V1
 */
interface $serviceName extends Service
{
    /**#@+
     * @const
     * @internal
     */
    const REQUEST          = $serviceRequest::class;
    const RESPONSE         = $serviceResponse::class;
    const HAS_SIDE_EFFECTS = false;

    /**
     * @param \\Cdt\\Service\\$entity\\$service\\$type\\V1\\$serviceRequest \$request
     * @return mixed
     */
    public function handleRequest($serviceRequest \$request);
}

EOT;
    }

    /**
     * @param $serviceEntity
     * @param $serviceService
     * @param $serviceType
     *
     * @return string
     */
    protected function generateServiceCoreTestHeader($serviceEntity, $serviceService, $serviceType)
    {
        $entity = $this->generateCanonicalName($serviceEntity);
        $service = $this->generateCanonicalName($serviceService);
        $type = $this->generateCanonicalName($serviceType);
        $serviceCoreName = $this->generateServiceCoreName(
            $serviceService,
            $serviceType
        );
        $serviceCoreTestName = $this->generateServiceCoreTestName(
            $serviceService,
            $serviceType
        );

        return <<<EOT
<?php
namespace Cdt\\Service\\$entity\\$service\\$type\\V1\\Tests;

use Cdt\\Common\\Testing\\Unit\\Service\\AbstractGenericGetEntityServiceTest;
use Cdt\\Entity\\$service\\$service;
use Cdt\\Gateway\\Simple\\SimpleCRDRepository;
use Cdt\\Service\\$entity\\$service\\$type\\V1\\$serviceCoreName;

class $serviceCoreTestName extends AbstractGenericGetEntityServiceTest
{
    public function getServiceClass()
    {
        return $serviceCoreName::class;
    }

    public function getRequestClass()
    {
        return $serviceCoreName::REQUEST;
    }

    public function getResponseClass()
    {
        return $serviceCoreName::RESPONSE;
    }

    public function getEntityClass()
    {
        return $service::class;
    }

    public function getRepositoryMock()
    {
        \$repository = \$this->getMockBuilder(SimpleCRDRepository::class)
                           ->disableOriginalConstructor()
                           ->getMock();

        return \$repository;
    }

    public function getReadQueryType()
    {
        return "simple";
    }

EOT;
    }

    protected function generateServiceCoreTestLookupData($serviceColumns)
    {
        $output = PHP_EOL . <<<EOT
    public function getSimpleLookupData()
    {
        return [
EOT;

        foreach ($serviceColumns as $requestColumn) {
            if (preg_match(
                "%^([a-zA-Z\_]+):([a-zA-Z]+):([a-zA-Z]+)%is",
                $requestColumn,
                $matches
            )) {
                $field = $this->generateCamelName($matches[1]);
                $module = $matches[3];
                $value = null;

                if ($module != "order") {
                    switch ($module) {
                        case "int":
                            $value = 1;
                            break;
                        case "tinyint":
                            $value = 1;
                            break;
                        case "varchar":
                            $value = '"example text #24d&^"';
                            break;
                        case "text":
                            $value = '"example text #24d&^"';
                            break;
                        case "timestamp":
                            $value = '"' . date("Y-m-d H:i:s", $this::TIME) . '"';
                            break;
                    }

                    $output .= PHP_EOL . <<<EOT
            ["$field", $value, "$field", "equalTo", $value],
EOT;
                }
            }
        }

        $output .= PHP_EOL . <<<EOT
        ];
    }

EOT;

        return $output;
    }

    protected function generateServiceCoreTestExampleResponse($serviceColumns)
    {
        $output = PHP_EOL . <<<EOT
    protected function getExampleReadResponse()
    {
        return [
EOT;

        foreach ($serviceColumns as $requestColumn) {
            if (preg_match(
                "%^([a-zA-Z\_]+):([a-zA-Z]+):([a-zA-Z]+)%is",
                $requestColumn,
                $matches
            )) {
                $field = $this->generateCamelName($matches[1]);
                $module = $matches[3];
                $value = null;

                if ($module != "order") {
                    switch ($module) {
                        case "int":
                            $value = 1;
                            break;
                        case "tinyint":
                            $value = 1;
                            break;
                        case "varchar":
                            $value = '"example text #24d&^"';
                            break;
                        case "text":
                            $value = '"example text #24d&^"';
                            break;
                        case "timestamp":
                            $value = '"' . date("Y-m-d H:i:s", $this::TIME) . '"';
                            break;
                    }

                    $output .= PHP_EOL . <<<EOT
            "$field" => $value,
EOT;
                }
            }
        }

        $output .= PHP_EOL . <<<EOT
        ];
    }

EOT;

        return $output;
    }

    protected function generateServiceCoreTestFooter()
    {
        return <<<EOT
}

EOT;
    }
}
