<?php
namespace Cdt\Console\Command\Generate;

use Cdt\Console\Command\AbstractCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class GenerateResponseCommand
 *
 * @package Cdt\Console\Command\Generate
 */
class GenerateResponseCommand extends AbstractCommand
{
    /**
     * @type string
     */
    protected $name = "generate:response";
    /**
     * @type string
     */
    protected $description = "Generate a service response based on given parameters";

    /**
     * @type array
     */
    protected $requiredDefinitions = [
        [
            "name"        => "entity",
            "description" => "Top level service namespace",
            "validation"  => "Please specify a top level service entity namespace",
            "default"     => null,
        ],
        [
            "name"        => "service",
            "description" => "Top level service entity",
            "validation"  => "Please specify a top level service name",
            "default"     => null,
        ],
        [
            "name"        => "type",
            "description" => "Service type",
            "validation"  => "Please specify a desired service type",
            "default"     => null,
        ],
        [
            "name"        => "path",
            "description" => "Path where to save the service relative to the cli call",
            "validation"  => "Please specify a path",
            "default"     => "src/Service/",
        ],
    ];

    /**
     * @internal
     */
    protected function configure()
    {
        parent::configure();

        $this->setHelp(
            "The <info>" . $this->name . "</info> command creates a service response from a given set of parameters"
        );
    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface   $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     *
     * @return bool
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->validateRequiredDefinitions($input, $output);

        if (! $this->error) {
            /**
             * @TODO fix type validation somewhere
             */
            $this->executeCommand(
                "generate:response:" . $input->getOption("type"),
                [
                    "--entity"  => $input->getOption("entity"),
                    "--service" => $input->getOption("service"),
                ],
                $output
            );
        }

        return ! $this->error;
    }
}
