<?php
namespace Cdt\Console\Command\Generate\Create;

use Cdt\Console\Application;
use Cdt\Console\Command\AbstractCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class GenerateServiceCreateCommand
 *
 * @package Cdt\Console\Command\Generate\Create
 */
class GenerateServiceCreateCommand extends AbstractCommand
{
    /**
     * The type of service, provided by the top level generate command
     */
    const TYPE = "create";
    /**
     * @type string
     */
    protected $name = "generate:service:create";
    /**
     * @type string
     */
    protected $description = "Generate a create service core based on given parameters";

    /**
     * @type array
     */
    protected $requiredDefinitions = [
        [
            "name"        => "entity",
            "description" => "Top level service namespace",
            "validation"  => "Please specify a top level service entity namespace",
            "default"     => null,
        ],
        [
            "name"        => "service",
            "description" => "Top level service entity",
            "validation"  => "Please specify a top level service name",
            "default"     => null,
        ],
        [
            "name"        => "path",
            "description" => "Path where to save the service relative to the cli call",
            "validation"  => "Please specify a path",
            "default"     => "src/Service/",
        ],
        [
            "name"        => "columns",
            "type"        => "argument",
            "class"       => 4,
            "description" => "Service entity columns",
            "validation"  => "Please specify all the columns divided by spaces",
            "default"     => null,
        ],
    ];

    /**
     * @internal
     */
    protected function configure()
    {
        parent::configure();

        $this->setHelp(
            "The <info>" . $this->name . "</info> command creates a create service core from a given set of parameters"
        );
    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface   $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     *
     * @return bool
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->validateRequiredDefinitions($input, $output);

        if (! $this->error) {
            $servicePath = $this->generateServicePath(
                $input->getOption("entity"),
                $input->getOption("service"),
                $this::TYPE
            );
            $serviceCorePath = $this->generateServiceCorePath(
                $input->getOption("entity"),
                $input->getOption("service"),
                $this::TYPE
            );
            $serviceCoreTestPath = $this->generateServiceCoreTestPath(
                $input->getOption("entity"),
                $input->getOption("service"),
                $this::TYPE
            );
            $serviceName = $this->generateServiceName(
                $input->getOption("service"),
                $this::TYPE
            );
            $serviceCoreName = $this->generateServiceCoreName(
                $input->getOption("service"),
                $this::TYPE
            );
            $serviceCoreTestName = $this->generateServiceCoreTestName(
                $input->getOption("service"),
                $this::TYPE
            );
            $serviceColumns = $input->getArgument("columns");

            if (! is_dir($servicePath)) {
                mkdir($servicePath);
            }
            $serviceStream = fopen(
                $servicePath . $serviceName . $this->generateExtension(),
                "w"
            );

            if ($serviceStream) {
                fwrite(
                    $serviceStream,
                    $this->generateService(
                        $input->getOption("entity"),
                        $input->getOption("service"),
                        $this::TYPE
                    )
                );
            }

            if (! is_dir($serviceCorePath)) {
                mkdir($serviceCorePath);
            }
            $serviceCoreStream = fopen(
                $serviceCorePath . $serviceCoreName . $this->generateExtension(),
                "w"
            );

            if ($serviceCoreStream) {
                fwrite(
                    $serviceCoreStream,
                    $this->generateServiceCoreHeader(
                        $input->getOption("entity"),
                        $input->getOption("service"),
                        $this::TYPE
                    )
                );

                fwrite(
                    $serviceCoreStream,
                    $this->generateServiceCoreFooter()
                );
            }

            if (! is_dir($serviceCoreTestPath)) {
                mkdir($serviceCoreTestPath);
            }
            $serviceCoreTestStream = fopen(
                $serviceCoreTestPath . $serviceCoreTestName . $this->generateExtension(),
                "w"
            );

            if ($serviceCoreTestStream) {
                fwrite(
                    $serviceCoreTestStream,
                    $this->generateServiceCoreTestHeader(
                        $input->getOption("entity"),
                        $input->getOption("service"),
                        $this::TYPE
                    )
                );

                fwrite(
                    $serviceCoreTestStream,
                    $this->generateFixtureData($serviceColumns)
                );

                fwrite(
                    $serviceCoreTestStream,
                    $this->generateServiceCoreTestMakeRequest(
                        $input->getOption("service"),
                        $this::TYPE
                    )
                );

                fwrite(
                    $serviceCoreTestStream,
                    $this->generateServiceCoreTestFooter()
                );
            }
        }

        return ! $this->error;
    }

    /**
     * @param $serviceEntity
     * @param $serviceService
     * @param $serviceType
     *
     * @return string
     */
    protected function generateServicePath($serviceEntity, $serviceService, $serviceType)
    {
        /**
         * @var Application $app
         */
        $app = $this->getApplication();
        $entity = $this->generateCanonicalName($serviceEntity);
        $service = $this->generateCanonicalName($serviceService);
        $type = $this->generateCanonicalName($serviceType);

        return $app->getRootPath() . "/src/Service/" . $entity . "/" . $service . "/" . $type . "/V1/";
    }

    /**
     * @param $serviceEntity
     * @param $serviceService
     * @param $serviceType
     *
     * @return string
     */
    protected function generateServiceCorePath($serviceEntity, $serviceService, $serviceType)
    {
        return $this->generateServicePath(
            $serviceEntity,
            $serviceService,
            $serviceType
        );
    }

    /**
     * @param $serviceEntity
     * @param $serviceService
     * @param $serviceType
     *
     * @return string
     */
    protected function generateServiceCoreTestPath($serviceEntity, $serviceService, $serviceType)
    {
        return $this->generateServicePath(
            $serviceEntity,
            $serviceService,
            $serviceType
        ) . "/Tests/";
    }

    /**
     * @param $serviceService
     * @param $serviceType
     *
     * @return string
     */
    protected function generateServiceName($serviceService, $serviceType)
    {
        $service = $this->generateCanonicalName($serviceService);
        $type = $this->generateCanonicalName($serviceType);

        return $type . $service . "Service";
    }

    /**
     * @param $serviceService
     * @param $serviceType
     *
     * @return string
     */
    protected function generateRequestName($serviceService, $serviceType)
    {
        $service = $this->generateCanonicalName($serviceService);
        $type = $this->generateCanonicalName($serviceType);

        return $type . $service . "Request";
    }

    /**
     * @param $serviceService
     * @param $serviceType
     *
     * @return string
     */
    protected function generateResponseName($serviceService, $serviceType)
    {
        $service = $this->generateCanonicalName($serviceService);
        $type = $this->generateCanonicalName($serviceType);

        return $type . $service . "Response";
    }

    /**
     * @param $serviceService
     * @param $serviceType
     *
     * @return string
     */
    protected function generateServiceCoreName($serviceService, $serviceType)
    {
        return $this->generateServiceName(
            $serviceService,
            $serviceType
        ) . "Core";
    }

    /**
     * @param $serviceService
     * @param $serviceType
     *
     * @return string
     */
    protected function generateServiceCoreTestName($serviceService, $serviceType)
    {
        return $this->generateServiceName(
            $serviceService,
            $serviceType
        ) . "CoreTest";
    }

    /**
     * @param $serviceEntity
     * @param $serviceService
     * @param $serviceType
     *
     * @return string
     */
    protected function generateServiceCoreHeader($serviceEntity, $serviceService, $serviceType)
    {
        $entity = $this->generateCanonicalName($serviceEntity);
        $service = $this->generateCanonicalName($serviceService);
        $type = $this->generateCanonicalName($serviceType);
        $serviceName = $this->generateServiceName(
            $serviceService,
            $serviceType
        );
        $serviceCoreName = $this->generateServiceCoreName(
            $serviceService,
            $serviceType
        );
        $serviceRequestName = $this->generateRequestName(
            $serviceService,
            $serviceType
        );
        $serviceResponseName = $this->generateResponseName(
            $serviceService,
            $serviceType
        );

        return <<<EOT
<?php
namespace Cdt\\Service\\$entity\\$service\\$type\\V1;

use Cdt\\Common\\Service\\AbstractService;
use Cdt\\Gateway\\Simple\\SimpleCreateRepository;
use Cdt\\Utils\\Event\\EventDispatcher;
use Cdt\\Utils\\Factory\\Factory;
use Cdt\Utils\Clock\Clock;
use Symfony\\Component\\Validator\\Validator\\ValidatorInterface;

/**
 * Class $serviceCoreName
 * @package Cdt\\Service\\$entity\\$service\\$type\\V1
 */
class $serviceCoreName extends AbstractService implements $serviceName
{
    /**
     * @type SimpleCreateRepository
     */
    private \$repository;
    /**
     * @type \\Cdt\\Utils\\Clock\\Clock
     */
    private \$clock;

    /**
     * @param EventDispatcher \$eventDispatcher
     * @param Factory \$responseFactory
     * @param ValidatorInterface  \$validator
     * @param SimpleCreateRepository \$repository
     * @param Clock \$clock
     */
    public function __construct(
        EventDispatcher \$eventDispatcher,
        Factory \$responseFactory,
        ValidatorInterface \$validator,
        SimpleCreateRepository \$repository,
        Clock \$clock
    ) {
        parent::__construct(
            \$eventDispatcher,
            \$responseFactory,
            \$validator
        );

        \$this->repository = \$repository;
        \$this->clock = \$clock;
    }

    /**
     * @param $serviceRequestName \$request
     *
     * @return $serviceResponseName
     */
    public function handleRequest($serviceRequestName \$request)
    {
        \$query = \$this->repository->newSimpleCreateQuery();

        \$dateCreated = \$request->getDateCreated();

        if (empty(\$dateCreated)) {
            \$request->setDateCreated(\$this->clock->getDateTime()->format("Y-m-d H:i:s"));
        }

        foreach (\$request->__toArray() as \$key => \$value) {
            \$query->simpleSet(
                \$key,
                \$value
            );
        }

        \$insert_id = \$this->repository->executeSimpleCreateQuery(\$query);

        return \$this->createResponse(\$insert_id);
    }

EOT;
    }

    /**
     * @return string
     */
    protected function generateServiceCoreFooter()
    {
        return <<<EOT
}

EOT;
    }

    /**
     * @param $serviceEntity
     * @param $serviceService
     * @param $serviceType
     *
     * @return string
     */
    protected function generateService($serviceEntity, $serviceService, $serviceType)
    {
        $entity = $this->generateCanonicalName($serviceEntity);
        $service = $this->generateCanonicalName($serviceService);
        $type = $this->generateCanonicalName($serviceType);
        $serviceName = $this->generateServiceName(
            $serviceService,
            $serviceType
        );
        $serviceRequest = $this->generateRequestName(
            $serviceService,
            $serviceType
        );
        $serviceResponse = $this->generateResponseName(
            $serviceService,
            $serviceType
        );

        return <<<EOT
<?php
namespace Cdt\\Service\\$entity\\$service\\$type\\V1;

use Cdt\\Common\\Service\\Service;

 /**
 * Interface $serviceName
 * The $serviceName service inserts an item of "$service" type in an "$entity" repository
 * @package Cdt\\Service\\$entity\\$service\\$type\\V1
 */
interface $serviceName extends Service
{
    /**#@+
     * @const
     * @internal
     */
    const REQUEST          = $serviceRequest::class;
    const RESPONSE         = $serviceResponse::class;
    const HAS_SIDE_EFFECTS = true;

    /**
     * @param \\Cdt\\Service\\$entity\\$service\\$type\\V1\\$serviceRequest \$request
     * @return mixed
     */
    public function handleRequest($serviceRequest \$request);
}

EOT;
    }

    /**
     * @param $serviceEntity
     * @param $serviceService
     * @param $serviceType
     *
     * @return string
     */
    protected function generateServiceCoreTestHeader($serviceEntity, $serviceService, $serviceType)
    {
        $entity = $this->generateCanonicalName($serviceEntity);
        $service = $this->generateCanonicalName($serviceService);
        $type = $this->generateCanonicalName($serviceType);
        $serviceCoreName = $this->generateServiceCoreName(
            $serviceService,
            $serviceType
        );
        $serviceCoreTestName = $this->generateServiceCoreTestName(
            $serviceService,
            $serviceType
        );
        $serviceRequestName = $this->generateRequestName(
            $serviceService,
            $serviceType
        );
        $serviceResponseName = $this->generateResponseName(
            $serviceService,
            $serviceType
        );

        return <<<EOT
<?php
namespace Cdt\\Service\\$entity\\$service\\$type\\V1\\Tests;

use Cdt\\Gateway\\Simple\\SimpleCreateRepository;
use Cdt\\Common\\Testing\\Unit\\Request\\RequestTestTrait;
use Cdt\\Gateway\\Query\\Simple\\SimpleCreateQuery;
use Cdt\\Utils\\Clock\\Clock;
use Cdt\\Utils\\Event\\EventDispatcher;
use Cdt\\Utils\\Factory\\Factory;
use Symfony\\Component\\Validator\\Validator\\ValidatorInterface;
use Cdt\\Service\\$entity\\$service\\$type\\V1\\$serviceRequestName;
use Cdt\\Service\\$entity\\$service\\$type\\V1\\$serviceResponseName;
use Cdt\\Service\\$entity\\$service\\$type\\V1\\$serviceCoreName;
use PHPUnit_Framework_TestCase;

class $serviceCoreTestName extends PHPUnit_Framework_TestCase
{
    use RequestTestTrait;

    /**
     * @type array
     */
    public \$validKeys;

EOT;
    }

    protected function generateFixtureData($serviceColumns)
    {
        $output = PHP_EOL . <<<EOT
    // @formatter:off
    public \$fixtureData = [
        "complete"   => [
            [[
EOT;

        foreach ($serviceColumns as $requestColumn) {
            if (preg_match(
                "%^([a-zA-Z\_]+):([a-zA-Z]+):([a-zA-Z]+)%is",
                $requestColumn,
                $matches
            )) {
                $field = $this->generateCamelName($matches[1]);
                $module = $matches[3];
                $value = null;

                if ($module != "order") {
                    switch ($module) {
                        case "int":
                            $value = 1;
                            break;
                        case "tinyint":
                            $value = 1;
                            break;
                        case "varchar":
                            $value = '"example text #24d&^"';
                            break;
                        case "text":
                            $value = '"example text #24d&^"';
                            break;
                        case "timestamp":
                            $value = '"' . date("Y-m-d H:i:s", $this::TIME) . '"';
                            break;
                    }

                    $output .= PHP_EOL . <<<EOT
                "$field" => $value,
EOT;
                }
            }
        }

        $output .= PHP_EOL . <<<EOT
            ]]
        ]
    ];
    // @formatter:on

    public function setUp()
    {
        \$valid = \$this->getValidRequests();

        \$this->validKeys = array_keys(\$valid[0][0]);
    }

    public function getValidRequests()
    {
        return \$this->fixtureData["complete"];
    }

EOT;

        return $output;
    }

    protected function generateServiceCoreTestMakeRequest($serviceService, $serviceType)
    {
        $serviceServiceCoreName = $this->generateServiceCoreName(
            $serviceService,
            $serviceType
        );
        $serviceRequestName = $this->generateRequestName(
            $serviceService,
            $serviceType
        );
        $serviceResponseName = $this->generateResponseName(
            $serviceService,
            $serviceType
        );

        $date = date("Y-M-d H:i:s", $this::TIME);

        $output = PHP_EOL . <<<EOT
    /**
     * @dataProvider getValidRequests
     */
    public function testMakeRequest(\$data)
    {
        \$eventDispatcher = \$this->getMock(EventDispatcher::class);
        \$responseFactory = \$this->getMockBuilder(Factory::class)->disableOriginalConstructor()->getMock();
        \$repository = \$this->getMockBuilder(SimpleCreateRepository::class)->disableOriginalConstructor()
                           ->getMock();
        \$request = \$this->getMockBuilder($serviceRequestName::class)
                        ->disableOriginalConstructor()->getMock();
        \$response = \$this->getMockBuilder($serviceResponseName::class)
                         ->disableOriginalConstructor()->getMock();
        \$clock = \$this->getMock(Clock::class);

        \$validator = \$this->getMock(ValidatorInterface::class);

        \$validator->expects(\$this->once())->method("validate")->with(\$request);

        \$responseFactory->expects(\$this->once())->method("create")->with(123)->will(\$this->returnValue(\$response));

        \$clock->expects(\$this->any())->method("getDateTime")->willReturnCallback(
            function () {
                return new \DateTime("$date");
            }
        );

        \$repository->expects(\$this->once())->method("newSimpleCreateQuery")->with()->willReturn(
            new SimpleCreateQuery()
        );

        \$expectedQuery = new SimpleCreateQuery();
        foreach (\$data as \$key => \$value) {
            \$expectedQuery->simpleSet(
                \$key,
                \$value
            );
        }

        \$request->expects(\$this->once())->method("__toArray")->will(\$this->returnValue(\$data));

        \$repository->expects(\$this->once())->method("executeSimpleCreateQuery")->with(\$expectedQuery)->willReturn(123);

        \$responseFactory->expects(\$this->once())->method("create")->with()->willReturn(\$response);

        \$service = new $serviceServiceCoreName(
            \$eventDispatcher,
            \$responseFactory,
            \$validator,
            \$repository,
            \$clock
        );

        \$this->assertEquals(\$response, \$service->makeRequest(\$request));
    }
EOT;

        return $output;
    }

    protected function generateServiceCoreTestFooter()
    {
        return PHP_EOL . <<<EOT
}

EOT;
    }
}
