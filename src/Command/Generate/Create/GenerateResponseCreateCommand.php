<?php
namespace Cdt\Console\Command\Generate\Create;

use Cdt\Console\Application;
use Cdt\Console\Command\AbstractCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateResponseCreateCommand extends AbstractCommand
{
    /**
     * The type of service, provided by the top level generate command
     */
    const TYPE = "create";
    /**
     * @type string
     */
    protected $name = "generate:response:create";
    /**
     * @type string
     */
    protected $description = "Generate a service create response based on given parameters";

    /**
     * @type array
     */
    protected $requiredDefinitions = [
        [
            "name"        => "entity",
            "description" => "Top level service namespace",
            "validation"  => "Please specify a top level service entity namespace",
            "default"     => null,
        ],
        [
            "name"        => "service",
            "description" => "Top level service entity",
            "validation"  => "Please specify a top level service name",
            "default"     => null,
        ],
        [
            "name"        => "path",
            "description" => "Path where to save the service relative to the cli call",
            "validation"  => "Please specify a path",
            "default"     => "src/Service/",
        ],
    ];

    /**
     * @internal
     */
    protected function configure()
    {
        parent::configure();

        $this->setHelp(
            "The <info>" . $this->name . "</info> command creates a service create response from a given set of parameters"
        );
    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface   $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     *
     * @return bool
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->validateRequiredDefinitions($input, $output);

        if (! $this->error) {
            $responsePath = $this->generateResponsePath(
                $input->getOption("entity"),
                $input->getOption("service"),
                $this::TYPE
            );
            $responseName = $this->generateResponseName(
                $input->getOption("service"),
                $this::TYPE
            );

            if (! is_dir($responsePath)) {
                mkdir($responsePath);
            }
            $responseStream = fopen(
                $responsePath . $responseName . $this->generateExtension(),
                "w"
            );

            if ($responseStream) {
                fwrite(
                    $responseStream,
                    $this->generateResponseHeader(
                        $input->getOption("entity"),
                        $input->getOption("service"),
                        $this::TYPE
                    )
                );

                fwrite(
                    $responseStream,
                    $this->generateResponseFooter()
                );
            }
        }

        return ! $this->error;
    }

    /**
     * @param $responseEntity
     * @param $responseService
     * @param $responseType
     *
     * @return string
     */
    protected function generateResponsePath($responseEntity, $responseService, $responseType)
    {
        /**
         * @var Application $app
         */
        $app = $this->getApplication();
        $entity = $this->generateCanonicalName($responseEntity);
        $service = $this->generateCanonicalName($responseService);
        $type = $this->generateCanonicalName($responseType);

        return $app->getRootPath() . "/src/Service/" . $entity . "/" . $service . "/" . $type . "/V1/";
    }

    /**
     * @param $responseService
     * @param $responseType
     *
     * @return string
     */
    protected function generateResponseName($responseService, $responseType)
    {
        $service = $this->generateCanonicalName($responseService);
        $type = $this->generateCanonicalName($responseType);

        return $type . $service . "Response";
    }

    /**
     * @param $responseEntity
     * @param $responseService
     * @param $responseType
     *
     * @return string
     */
    protected function generateResponseHeader($responseEntity, $responseService, $responseType)
    {
        $entity = $this->generateCanonicalName($responseEntity);
        $service = $this->generateCanonicalName($responseService);
        $type = $this->generateCanonicalName($responseType);
        $responseName = $this->generateResponseName(
            $responseService,
            $responseType
        );

        return <<<EOT
<?php
namespace Cdt\\Service\\$entity\\$service\\$type\\V1;

use Cdt\\Common\\Response\\Response;

/**
 * Class $responseName
 * @package Cdt\\Service\\$entity\\$service\\$type\\V1
 */
class $responseName implements Response
{
    /**
     * @type int
     */
    private \$insertId;

    /**
     * @param \$insertId
     */
    public function __construct(\$insertId)
    {
        \$this->insertId = \$insertId;
    }

    /**
     * @return int
     */
    public function getInsertId()
    {
        return \$this->insertId;
    }
EOT;
    }

    /**
     * @return string
     */
    protected function generateResponseFooter()
    {
        return PHP_EOL . <<<EOT
}

EOT;
    }
}
