<?php
namespace Cdt\Console\Command\Generate;

use Cdt\Console\Application;
use Cdt\Console\Command\AbstractCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class GenerateStructureCommand
 *
 * @package Cdt\Console\Command\Generate
 */
class GenerateStructureCommand extends AbstractCommand
{
    /**
     * @type string
     */
    protected $name = "generate:structure";
    /**
     * @type string
     */
    protected $description = "Generate a service repository structure based on given parameters";

    /**
     * @type array
     */
    protected $requiredDefinitions = [
        [
            "name"        => "service",
            "description" => "Top level service entity",
            "validation"  => "Please specify a top level service name for the structure",
            "default"     => null,
        ],
        [
            "name"        => "columns",
            "type"        => "argument",
            "class"       => 4,
            "description" => "Service structure columns",
            "validation"  => "Please specify all the columns divided by spaces",
            "default"     => null,
        ],
    ];

    /**
     * @internal
     */
    protected function configure()
    {
        parent::configure();

        $this->setHelp(
            "The <info>" . $this->name . "</info> command creates a service structure from a given set of parameters"
        );
    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface   $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     *
     * @return bool
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->validateRequiredDefinitions($input, $output);

        if (! $this->error) {
            $structurePath = $this->generateStructurePath();
            $structureName = $this->generateStructureName($input->getOption("service"));
            $structureColumns = $input->getArgument("columns");

            if (! is_dir($structurePath)) {
                mkdir($structurePath);
            }
            $structureStream = fopen(
                $structurePath . $structureName . $this->generateExtension(),
                "w"
            );

            if ($structureStream) {
                fwrite(
                    $structureStream,
                    $this->generateStructureHeader($structureName)
                );

                foreach ($structureColumns as $structureColumn) {
                    fwrite(
                        $structureStream,
                        $this->generateStructureColumn($structureColumn)
                    );
                }

                fwrite(
                    $structureStream,
                    $this->generateStructureFooter()
                );
            }
        }

        return ! $this->error;
    }

    /**
     * @return string
     */
    protected function generateStructurePath()
    {
        /**
         * @var Application $app
         */
        $app = $this->getApplication();

        return $app->getRootPath() . "/src/Gateway/Structure/Structures/";
    }

    /**
     * @param $name
     *
     * @return string
     */
    protected function generateStructureName($name)
    {
        return $this->generateCanonicalName($name) . "RepositoryStructure";
    }

    /**
     * @param $structureName
     *
     * @return string
     */
    protected function generateStructureHeader($structureName)
    {
        return <<<EOT
<?php
namespace Cdt\\Gateway\\Structure\\Structures;

use Cdt\\Gateway\\Structure\\Field;
use Cdt\\Gateway\\Structure\\RepositoryStructure;
use Cdt\\Gateway\\Structure\\RepositoryStructureTrait;

class $structureName implements RepositoryStructure
{
    use RepositoryStructureTrait;

    protected function setupFields()
    {
        return [

EOT;
    }

    /**
     * @param $structureColumnType
     *
     * @return string
     */
    protected function generateStructureColumn($structureColumnType)
    {
        if (preg_match(
            "%^([a-zA-Z\_]+):([a-zA-Z]+)$%is",
            $structureColumnType,
            $matches
        )) {
            return $this->writeStructureColumn(
                $matches[1],
                $matches[2]
            );
        }

        return false;
    }

    /**
     * @param        $structureColumn
     * @param string $type
     *
     * @return string
     */
    protected function writeStructureColumn($structureColumn, $type = "")
    {
        return <<<EOT
            "$structureColumn" => new Field("$type", Field::NULLABLE),

EOT;
    }

    /**
     * @return string
     */
    protected function generateStructureFooter()
    {
        return <<<EOT
        ];
    }
}

EOT;
    }
}
