<?php
namespace Cdt\Console\Command\Generate;

use Cdt\Console\Application;
use Cdt\Console\Command\AbstractCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class GenerateInitCommand
 *
 * @package Cdt\Console\Command\Generate
 */
class GenerateInitCommand extends AbstractCommand
{
    /**
     * @type string
     */
    protected $name = "generate:init";
    /**
     * @type string
     */
    protected $description = "Initialise a service boilerplate";
    /**
     * @type array
     */
    protected $dirStructure = [
        "{Entity}" => [
            "{Service}" => [
                "{Type}" => [
                    "V1" => [
                        "Features" => [
                            "{Type}{Service}.feature"
                        ],
                        "Tests"    => [
                            "{Type}{Service}RequestTest.php",
                            "{Type}{Service}ServiceCoreTest.php",
                        ],
                        "{Type}{Service}Request.php",
                        "{Type}{Service}Response.php",
                        "{Type}{Service}Service.php",
                        "{Type}{Service}ServiceCore.php",
                    ]
                ]
            ]
        ]
    ];
    /**
     * @type array
     */
    protected $requiredDefinitions = [
        [
            "name"        => "entity",
            "description" => "Top level service namespace",
            "validation"  => "Please specify a top level service entity namespace",
            "default"     => null,
        ],
        [
            "name"        => "service",
            "description" => "Top level service entity",
            "validation"  => "Please specify a top level service name",
            "default"     => null,
        ],
        [
            "name"        => "type",
            "description" => "Service type",
            "validation"  => "Please specify a desired service type",
            "default"     => null,
        ],
        [
            "name"        => "path",
            "description" => "Path where to save the service relative to the cli call",
            "validation"  => "Please specify a path",
            "default"     => "src/Service/",
        ],
    ];

    /**
     * @internal
     */
    protected function configure()
    {
        parent::configure();

        $this->setHelp(
            "The <info>" . $this->name . "</info> command creates a new service boilerplate structure with dummy files and folders"
        );
    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface   $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     *
     * @return bool
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $error = false;
        /**
         * @var Application $app
         */
        $app = $this->getApplication();
        $optionValues = $this->validateRequiredDefinitions(
            $input,
            $output
        );

        if (! $error) {
            if ($output->isDebug()) {
                $output->writeln("<info>Attempting to create service structure:</info>");
            }

            $this->callbackStructureIterator(
                $output,
                $this->dirStructure,
                function ($name, $isFolder, $treeSpace, $treeName) use ($app, $input, $output, $optionValues) {
                    $parsedFolder = str_replace(
                        array_keys($optionValues),
                        array_values($optionValues),
                        $treeName . "/" . ($isFolder ? $name : '')
                    );
                    $folder = $app->getRootPath() . "/" . $input->getOption("path") . $parsedFolder;
                    $parsedFilename = str_replace(
                        array_keys($optionValues),
                        array_values($optionValues),
                        $name
                    );
                    $filename = $folder . $parsedFilename;

                    if ($isFolder) {
                        $success = is_dir($folder) ? : mkdir($folder);
                    } else {
                        $success = fopen(
                            $filename,
                            'w'
                        );
                    }

                    if ($output->isVeryVerbose()) {
                        $output->writeln(
                            ($success ? "<comment>" : "<error>") . $treeSpace . $parsedFilename . ($isFolder ? '/' : '') . ($success ? "</comment>" : "</error>")
                        );
                    }
                }
            );
        }

        return ! $error;
    }

    /**
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     * @param                                                   $array
     * @param                                                   $callback
     * @param int                                               $treePos
     * @param string                                            $treeName
     */
    protected function callbackStructureIterator(
        OutputInterface $output,
        $array,
        $callback,
        $treePos = 0,
        $treeName = ''
    ) {
        foreach ($array as $key => $value) {
            $isFolder = true;
            $name = $value;
            $treeSpace = '';

            for ($i = 0; $i < $treePos; $i++) {
                $treeSpace .= "  ";
            }

            if (is_array($value)) {
                $name = $key;
            } else {
                $isFolder = false;
            }

            $treeSpace .= "┗━ ";

            if (is_callable($callback)) {
                $callback(
                    $name,
                    $isFolder,
                    $treeSpace,
                    $treeName
                );
            }

            if (is_array($value)) {
                $this->callbackStructureIterator(
                    $output,
                    $value,
                    $callback,
                    $treePos + 1,
                    $treeName . (is_array($value) ? $name . "/" : '')
                );
            }
        }
    }
}
