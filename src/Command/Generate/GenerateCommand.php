<?php
namespace Cdt\Console\Command\Generate;

use Cdt\Console\Application;
use Cdt\Console\Command\AbstractCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\Container;
use Yosymfony\ConfigLoader\Repository;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Metadata\Metadata;
use Zend\Db\Metadata\Object\ColumnObject;

/**
 * Class GenerateCommand
 *
 * @package Cdt\Console\Command\Generate
 */
class GenerateCommand extends AbstractCommand
{
    /**
     * @type string
     */
    protected $name = "generate";
    /**
     * @type string
     */
    protected $description = "Generate a set of services based on a config.json configuration";

    /**
     * @internal
     */
    protected function configure()
    {
        parent::configure();

        $this->setHelp(
            "The <info>" . $this->name . "</info> command creates a set of services from a given configuration"
        );
    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface   $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     *
     * @return bool
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /**
         * @var Application $app
         * @var Repository $config
         */
        $error = $metadata = false;
        $message = "";
        $app = $this->getApplication();
        $config = $app->getConfig();
        $globalConfig = $config->get("global");
        $dbConfig = $config->get("db");
        $entities = $config->get("entities");

        if (false === $globalConfig) {
            $error = true;
            $message = "Config is missing global config";
        } elseif (false === $dbConfig) {
            $error = true;
            $message = "Config is missing database connections";
        } else {
            $adapter = new Adapter($dbConfig);
            try {
                $metadata = new Metadata($adapter);
            } catch (\Exception $e) {
                $error = true;
                $message = $e->getMessage();
            }
        }

        if (! $error) {
            if (false === $entities || (! isset($globalConfig["generator"]) || false === $globalConfig["generator"])) {
                $error = true;
                $message = "Config is missing services to generate";
            } else {
                foreach ($entities as $entity) {
                    foreach ($entity["services"] as $service) {
                        if (isset($service["disabled"]) && $service["disabled"] == true) {
                            if ($output->isVerbose()) {
                                $output->writeln("<info>Service <comment>{$service["name"]}</comment> exists in the config but generation is disabled</info>");
                            }

                            continue;
                        }

                        $service["name"] = Container::camelize($service["name"]);
                        $entityColumns = $requestColumns = $serviceColumns = [];

                        $output->writeln("");
                        $output->writeln("Trying to create <comment>" . $service["name"] . "</comment> service");

                        foreach ($service["schema"] as $schema) {
                            $cols = $metadata->getColumns($schema);

                            /**
                             * @var ColumnObject $column
                             */
                            foreach ($cols as $column) {
                                $serviceColumns[] = $this->mapServiceColumn($column);
                                $entityColumns[] = $this->mapEntityColumn($column);
                                $requestColumns[] = $this->mapRequestColumn($column);
                            }
                        }

                        $generateEntityCommand = $this->executeCommand(
                            "generate:entity",
                            [
                                "--service" => $service["name"],
                                "columns"   => $entityColumns
                            ],
                            $output
                        );

                        $generateStructureCommand = $this->executeCommand(
                            "generate:structure",
                            [
                                "--service" => $service["name"],
                                "columns"   => $entityColumns
                            ],
                            $output
                        );

                        if ($generateEntityCommand === 0 && $generateStructureCommand === 0) {
                            foreach ($service["types"] as $serviceType) {
                                $this->executeCommand(
                                    "generate:init",
                                    [
                                        "--entity"  => $entity["namespace"],
                                        "--service" => $service["name"],
                                        "--type"    => $serviceType["type"],
                                    ],
                                    $output
                                );

                                $this->executeCommand(
                                    "generate:request",
                                    [
                                        "--entity"  => $entity["namespace"],
                                        "--service" => $service["name"],
                                        "--type"    => $serviceType["type"],
                                        "columns"   => $requestColumns
                                    ],
                                    $output
                                );

                                $this->executeCommand(
                                    "generate:response",
                                    [
                                        "--entity"  => $entity["namespace"],
                                        "--service" => $service["name"],
                                        "--type"    => $serviceType["type"],
                                    ],
                                    $output
                                );

                                $this->executeCommand(
                                    "generate:service",
                                    [
                                        "--entity"  => $entity["namespace"],
                                        "--service" => $service["name"],
                                        "--type"    => $serviceType["type"],
                                        "columns"   => $serviceColumns,
                                    ],
                                    $output
                                );

                                continue;
                            }
                        }
                    }
                }
            }
        }

        $output->writeln("<error>" . $message . "</error>");

        return ! $error;
    }

    /**
     * @param \Zend\Db\Metadata\Object\ColumnObject $column
     *
     * @return string
     */
    protected function mapEntityColumn(ColumnObject $column)
    {
        return $column->getName() . ":" . $this->writeMapType($column->getDataType());
    }

    /**
     * @param \Zend\Db\Metadata\Object\ColumnObject $column
     *
     * @return string
     */
    protected function mapRequestColumn(ColumnObject $column)
    {
        $arguments = [
            $column->getName(),
            $this->writeMapType($column->getDataType()),
            strtolower($column->getDataType())
        ];

        // @formatter:off
        return implode(":", $arguments);
        // @formatter:on
    }

    /**
     * @param \Zend\Db\Metadata\Object\ColumnObject $column
     *
     * @return string
     */
    protected function mapServiceColumn(ColumnObject $column)
    {
        $arguments = [
            $column->getName(),
            $this->writeMapType($column->getDataType()),
            strtolower($column->getDataType())
        ];

        // @formatter:off
        return implode(":", $arguments);
        // @formatter:on
    }
}
