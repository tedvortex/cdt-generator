<?php
namespace Cdt\Console\Command\Generate;

use Cdt\Console\Command\AbstractCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class GenerateRequestCommand
 *
 * @package Cdt\Console\Command\Generate
 */
class GenerateRequestCommand extends AbstractCommand
{
    /**
     * @type string
     */
    protected $name = "generate:request";
    /**
     * @type string
     */
    protected $description = "Generate a service request based on given parameters";

    /**
     * @type array
     */
    protected $requiredDefinitions = [
        [
            "name"        => "entity",
            "description" => "Top level service namespace",
            "validation"  => "Please specify a top level service entity namespace",
            "default"     => null,
        ],
        [
            "name"        => "service",
            "description" => "Top level service entity",
            "validation"  => "Please specify a top level service name",
            "default"     => null,
        ],
        [
            "name"        => "type",
            "description" => "Service type",
            "validation"  => "Please specify a desired service type",
            "default"     => null,
        ],
        [
            "name"        => "path",
            "description" => "Path where to save the service relative to the cli call",
            "validation"  => "Please specify a path",
            "default"     => "src/Service/",
        ],
        [
            "name"        => "columns",
            "type"        => "argument",
            "class"       => 4,
            "description" => "Service entity columns",
            "validation"  => "Please specify all the columns divided by spaces",
            "default"     => null,
        ],
    ];

    /**
     * @internal
     */
    protected function configure()
    {
        parent::configure();

        $this->setHelp(
            "The <info>" . $this->name . "</info> command creates a service request from a given set of parameters"
        );
    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface   $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     *
     * @return bool
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->validateRequiredDefinitions($input, $output);

        if (! $this->error) {
            $requestColumns = $requestTestColumns = $setterColumns = $getterColumns = $validationColumns = $input->getArgument("columns");

            /**
             * @TODO fix type validation somewhere
             */
            $this->executeCommand(
                "generate:request:" . $input->getOption("type"),
                [
                    "--entity"  => $input->getOption("entity"),
                    "--service" => $input->getOption("service"),
                    "columns"   => $requestColumns
                ],
                $output
            );
        }

        return ! $this->error;
    }
}
