<?php
namespace Cdt\Console\Command\Generate;

use Cdt\Console\Application;
use Cdt\Console\Command\AbstractCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\Container;

/**
 * Class GenerateEntityCommand
 *
 * @package Cdt\Console\Command\Generate
 */
class GenerateEntityCommand extends AbstractCommand
{
    /**
     * @type string
     */
    protected $name = "generate:entity";
    /**
     * @type string
     */
    protected $description = "Generate a service entity based on given parameters";

    /**
     * @type array
     */
    protected $requiredDefinitions = [
        [
            "name"        => "service",
            "description" => "Top level service entity",
            "validation"  => "Please specify a top level service name for the entity",
            "default"     => null,
        ],
        [
            "name"        => "columns",
            "type"        => "argument",
            "class"       => 4,
            "description" => "Service entity columns",
            "validation"  => "Please specify all the columns divided by spaces",
            "default"     => null,
        ],
    ];

    /**
     * @internal
     */
    protected function configure()
    {
        parent::configure();

        $this->setHelp(
            "The <info>" . $this->name . "</info> command creates a service entity from a given set of parameters"
        );
    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface   $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     *
     * @return bool
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->validateRequiredDefinitions(
            $input,
            $output
        );

        if (! $this->error) {
            $entityPath = $this->generateEntityPath($input->getOption("service"));
            $entityTestPath = $this->generateEntityTestPath($input->getOption("service"));
            $entityName = $this->generateEntityName($input->getOption("service"));
            $entityTestName = $this->generateEntityTestName($input->getOption("service"));
            $entityColumns = $input->getArgument("columns");

            if (! is_dir($entityPath)) {
                mkdir($entityPath);
            }
            $entityStream = fopen(
                $entityPath . $entityName . $this->generateExtension(),
                "w"
            );

            if ($entityStream) {
                fwrite(
                    $entityStream,
                    $this->generateEntityHeader($entityName)
                );

                foreach ($entityColumns as $entityColumn) {
                    fwrite(
                        $entityStream,
                        $this->generateEntityColumn($entityColumn)
                    );
                }

                foreach ($entityColumns as $entityColumn) {
                    fwrite(
                        $entityStream,
                        $this->generateEntityGetter($entityColumn)
                    );
                }

                foreach ($entityColumns as $entityColumn) {
                    fwrite(
                        $entityStream,
                        $this->generateEntitySetter($entityColumn)
                    );
                }

                fwrite(
                    $entityStream,
                    $this->generateEntityFooter()
                );
            }

            if (! is_dir($entityTestPath)) {
                mkdir($entityTestPath);
            }
            $entityTestStream = fopen(
                $entityTestPath . $entityTestName . $this->generateExtension(),
                "w"
            );

            if ($entityTestStream) {
                fwrite(
                    $entityTestStream,
                    $this->generateEntityTest(
                        $entityName,
                        $entityTestName
                    )
                );
            }
        }

        return ! $this->error;
    }

    /**
     * @param $entityName
     *
     * @return string
     */
    protected function generateEntityPath($entityName)
    {
        /**
         * @var Application $app
         */
        $app = $this->getApplication();

        return $app->getRootPath() . "/src/Entity/" . $this->generateCanonicalName($entityName) . "/";
    }

    /**
     * @param $entityName
     *
     * @return string
     */
    protected function generateEntityTestPath($entityName)
    {
        /**
         * @var Application $app
         */
        $app = $this->getApplication();

        return $app->getRootPath() . "/src/Entity/" . $this->generateCanonicalName($entityName) . "/Tests/";
    }

    /**
     * @param $name
     *
     * @return string
     */
    protected function generateEntityName($name)
    {
        return $this->generateCanonicalName($name);
    }

    /**
     * @param $name
     *
     * @return string
     */
    protected function generateEntityTestName($name)
    {
        return $this->generateCanonicalName($name) . "Test";
    }

    /**
     * @param $entityName
     *
     * @return string
     */
    protected function generateEntityHeader($entityName)
    {
        return <<<EOT
<?php
namespace Cdt\\Entity\\$entityName;

use Cdt\\Entity\\Helpers\\ArrayInitializerTrait;

class $entityName
{
    use ArrayInitializerTrait;

EOT;
    }

    /**
     * @param $entityColumnType
     *
     * @return string
     */
    protected function generateEntityColumn($entityColumnType)
    {
        if (preg_match(
            "%^([a-zA-Z\_]+):([a-zA-Z]+)$%is",
            $entityColumnType,
            $matches
        )) {
            return $this->writeEntityColumn(
                $matches[1],
                $matches[2]
            );
        }

        return false;
    }

    /**
     * @param        $entityColumn
     * @param string $type
     *
     * @return string
     */
    protected function writeEntityColumn($entityColumn, $type = "")
    {
        return <<<EOT

    /**
     * @type $type
     */
    private \$$entityColumn;

EOT;
    }

    /**
     * @param $entityColumnType
     *
     * @return bool|string
     */
    protected function generateEntityGetter($entityColumnType)
    {
        if (preg_match(
            "%^([a-zA-Z\_]+):([a-zA-Z]+)$%is",
            $entityColumnType,
            $matches
        )) {
            return $this->writeEntityGetter(
                $matches[1],
                $matches[2]
            );
        }

        return false;
    }

    /**
     * @param $entityColumn
     * @param $type
     *
     * @return string
     */
    protected function writeEntityGetter($entityColumn, $type)
    {
        $entityColumnCamel = Container::camelize($entityColumn);

        return <<<EOT

    /**
     * @return $type
     */
    public function get$entityColumnCamel()
    {
        return \$this->$entityColumn;
    }

EOT;
    }

    /**
     * @param $entityColumnType
     *
     * @return bool|string
     */
    protected function generateEntitySetter($entityColumnType)
    {
        if (preg_match(
            "%^([a-zA-Z\_]+):([a-zA-Z]+)$%is",
            $entityColumnType,
            $matches
        )) {
            return $this->writeEntitySetter(
                $matches[1],
                $matches[2]
            );
        }

        return false;
    }

    /**
     * @param $entityColumn
     * @param $type
     *
     * @return string
     */
    protected function writeEntitySetter($entityColumn, $type)
    {
        $entityColumnCamel = Container::camelize($entityColumn);

        return <<<EOT

    /**
     * @param $type \$$entityColumn
     */
    public function set$entityColumnCamel(\$$entityColumn)
    {
        \$this->$entityColumn = \$$entityColumn;
    }

EOT;
    }

    /**
     * @return string
     */
    protected function generateEntityFooter()
    {
        return <<<EOT
}

EOT;
    }

    /**
     * @param $entityName
     * @param $entityTestName
     *
     * @return string
     */
    protected function generateEntityTest($entityName, $entityTestName)
    {
        return <<<EOT
<?php
namespace Cdt\\Entity\\$entityName\\Tests;

use Cdt\\Common\\Testing\\Unit\\GettersAndSettersTestTrait;
use Cdt\\Entity\\$entityName\\$entityName;
use PHPUnit_Framework_TestCase;

class $entityTestName extends PHPUnit_Framework_TestCase
{
    use GettersAndSettersTestTrait;

    protected function getClassToTest()
    {
        return $entityName::class;
    }
}

EOT;
    }
}
