<?php
namespace Cdt\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\Container;

/**
 * Class AbstractCommand
 *
 * @package Cdt\Console\Command
 */
abstract class AbstractCommand extends Command
{
    /**
     * Sample timestamp for keeping a consistent date between regenerations
     * @internal
     */
    const TIME = 1432415551;
    /**
     * @type string
     */
    protected $name;
    /**
     * @type string
     */
    protected $description;
    /**
     * @type
     */
    protected $root;
    /**
     * @type array
     */
    protected $requiredDefinitions = [];
    /**
     * @type bool
     */
    protected $error = false;

    /**
     * Generic set up function
     */
    protected function configure()
    {
        $this->setName($this->name)
             ->setDescription($this->description);

        foreach ($this->requiredDefinitions as $definition) {
            $type = (isset($definition["type"]) ? $definition["type"] : "option");
            $addType = $this->validateTypeFunction($type);

            switch ($type) {
                case "option":
                    $this->$addType(
                        $definition["name"],
                        null,
                        (isset($definition["class"]) ? $definition["class"] : InputOption::VALUE_REQUIRED),
                        $definition["description"],
                        $definition["default"]
                    );
                    break;

                case "argument":
                    $this->$addType(
                        $definition["name"],
                        (isset($definition["class"]) ? $definition["class"] : InputOption::VALUE_REQUIRED),
                        $definition["description"],
                        $definition["default"]
                    );
                    break;
            }
        }
    }

    /**
     * @return string
     */
    protected function generateExtension()
    {
        return ".php";
    }

    /**
     * @param string $name
     *
     * @return string
     */
    protected function generateCanonicalName($name = '')
    {
        // @formatter:off
        return strtoupper(substr($name, 0, 1)) . substr($name, 1);
        // @formatter:on
    }

    /**
     * @param string $name
     *
     * @return string
     */
    protected function generateCamelName($name = '')
    {
        // @formatter:off
        return strtolower($name{0}) . substr(Container::camelize($name), 1);
        // @formatter:on
    }

    /**
     * @param        $type
     * @param string $function
     *
     * @return string
     */
    protected function validateTypeFunction($type, $function = "add")
    {
        return $function . mb_convert_case(
            $type,
            MB_CASE_TITLE
        );
    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface   $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     *
     * @return array
     */
    protected function validateRequiredDefinitions(InputInterface $input, OutputInterface $output)
    {
        $message = "";
        $optionValues = [];

        foreach ($this->requiredDefinitions as $definition) {
            $type = (isset($definition["type"]) ? $definition["type"] : "option");
            $getType = $this->validateTypeFunction(
                $type,
                "get"
            );

            if (! $input->$getType($definition["name"])) {
                $this->error = true;

                switch ($type) {
                    case "option":
                        $message = " (--" . $definition["name"] . "=\"\")";
                        break;

                    case "argument":
                        $message = " (" . $definition["name"] . "1 ... " . $definition["name"] . "N)";
                        break;
                }

                $output->writeln("<error>" . $definition["validation"] . $message . "</error>");
            } else {
                if ($type == "option") {
                    // @formatter:off
                    $key = "{" . mb_convert_case($definition["name"], MB_CASE_TITLE) . "}";
                    // @formatter:on
                    $optionValues[$key] = $this->generateCanonicalName($input->$getType($definition["name"]));
                }
            }
        }

        return $optionValues;
    }

    /**
     * @param $doctrineType
     *
     * @return string
     */
    protected function writeMapType($doctrineType)
    {
        $type = "string";

        switch ($doctrineType) {
            case "int":
                $type = "integer";
                break;
            case "tinyint":
                $type = "integer";
                break;
            case "timestamp":
                $type = "string";
                break;
            case "varchar":
                $type = "string";
                break;
            case "text":
                $type = "string";
                break;
        }

        return $type;
    }

    /**
     * @param                                                   $commandName
     * @param                                                   $commandArguments
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     *
     * @return bool|int
     * @throws \Exception
     */
    protected function executeCommand($commandName, $commandArguments, OutputInterface $output)
    {
        $command = $this->getApplication()
                        ->find($commandName);

        $cmd = "php bin/console " . $commandName;
        $cmdArgs = " ";

        foreach ($commandArguments as $key => $argument) {
            // @formatter:off
            $format = (is_array($argument) ? implode(" ", $argument) : $key . "=" . $argument);
            // @formatter:on
            $cmdArgs .= $format . " ";
        }

        if ($output->isDebug()) {
            $output->writeln("<info>Attempting to run command:</info>");
        }

        if ($output->isVeryVerbose()) {
            $output->writeln("<comment>" . $cmd . $cmdArgs . "</comment>");
        }

        if ($command) {
            $commandArguments["command"] = $commandName;
            $input = new ArrayInput($commandArguments);

            $commandCode = $command->run(
                $input,
                $output
            );

            if ($commandCode) {
                $output->writeln("<error>Command failed with code: " . $commandCode . "</error>");

                return false;
            }

            if ($output->isDebug()) {
                $output->writeln("<info>Command executed with status code: " . $commandCode . "</info>");
            }

            return $commandCode;
        }

        return false;
    }
}
